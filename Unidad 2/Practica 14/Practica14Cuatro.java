public class Practica14Cuatro{
   public static void main(String[] args){
      Practica14 p1 = new Practica14();
      p1.start();
      
      Practica14Dos p2 = new Practica14Dos();
      Practica14Dos p2copia = new Practica14Dos();
      p2.start();
      p2copia.start();
      
      Practica14Tres p3 = new Practica14Tres();
      Practica14Tres p3copia = new Practica14Tres();
      p3.start();
      p3copia.start();
      
   }
}