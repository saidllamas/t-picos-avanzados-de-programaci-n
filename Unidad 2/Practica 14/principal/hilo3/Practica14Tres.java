package principal.hilo3;

import java.awt.*;
import java.applet.*;

/**
*  <h1>Practica 14, ejercicio 3 </h1> <br>
*  <p>Implementando la interfaz Runnable se imprimen n cantidad de veces (generadas por la clase Math) *</p>
*/
public class Practica14Tres extends Applet implements Runnable{
   
   /**Hilo global*/
   Thread t;
   /**Bandera que me ayuda a identificar el estado del hilo*/
   boolean ban = false;
   
   /**Metodo default de applet*/
   public void init(){
      
   }
   
   /**Metodo inicial de la interfaz Runnable*/
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   /**Metodo final de la interfaz Runnable*/
   public void stop(){
      ban = true;
      t = null;
   }
   
   /**El siguiente metodo me ayuda a pausar el programa X milisegundos*/
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   /**Metodo para inciar hilo*/
   public void run(){
      int random = (int)Math.floor(Math.random()*20);
      //System.out.println(random);
      for(int i = 1; i <= random; i++) System.out.println("*");
   }
}