package principal;

import principal.hilo1.*;
import principal.hilo2.*;
import principal.hilo3.*;

/**
*  <h1>Practica 14 </h1> <br>
*  <p>Esta es la clase principal del programa. Inicia tres hilos diferentes </p>
*  @author Jesus Said Llamas Manriquez
*  @version Estable v1
*/
public class Practica14Cinco{
   /** @param  args Array de argumentos por default*/
   public static void main(String[] args){
      Practica14 p1 = new Practica14();
      p1.start();
      
      Practica14Dos p2 = new Practica14Dos();
      Practica14Dos p2copia = new Practica14Dos();
      p2.start();
      p2copia.start();
      
      Practica14Tres p3 = new Practica14Tres();
      Practica14Tres p3copia = new Practica14Tres();
      p3.start();
      p3copia.start();
      
   }
}