package principal.hilo2;

/**
*  <h1>Practica 14, ejercicio 2 </h1> <br>
*  <p>Imprime 10 veces el mensaje �Feliz dia del maestro! </p>
*/
public class Practica14Dos extends Thread{

   /** Metodo para correr el hilo*/
   public void run(){
      for(int i = 1; i <= 10; i++){
         System.out.println(" �Feliz dia del maestro!");
      }
   }   
   
   /** @param  args Array de argumentos*/
   public static void main(String[] args){
      Practica14Dos app = new Practica14Dos();
      app.start();
   }
}