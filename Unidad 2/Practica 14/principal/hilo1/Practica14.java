package principal.hilo1;

import java.awt.*;
import java.applet.*;

/**
*  <h1>Practica 14, ejercicio 1 </h1> <br>
*  <p>Imprime todos los numeros divisibles entre 2 del 1 al 50. </p>
*/
public class Practica14 extends Applet implements Runnable{
   
   Thread t;
   boolean ban = false;
   
   /**Metodo que inicia con el applet*/
   public void init(){
      
   }
   
   /**Metodo cuando inicia el hilo*/
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   /**Metodo cuando se detiene el hilo*/
   public void stop(){
      ban = true;
      t = null;
   }
   
   /**Metodo cuando se pausa el programa*/
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   /**Metodo cuando corre el hilo*/
   public void run(){
      for(int i = 1; i <= 50; i++){
         if((i%2) == 0) System.out.println(i);
      }
   }
}