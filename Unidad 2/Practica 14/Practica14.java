import java.awt.*;
import java.applet.*;

public class Practica14 extends Applet implements Runnable{
   
   Thread t;
   boolean ban = false;
   
   public void init(){
      
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      for(int i = 1; i <= 50; i++){
         if((i%2) == 0) System.out.println(i);
      }
   }
}