package ventas;

import java.awt.*;
import java.awt.event.*;

/**
*	Programa desarrollado por Jesus Said Llamas Manriquez 02/04/2017 como Practica #11
*	para la materia de topicos avanzados de programacion.
*	El siguiente construye un frame que simula una pantalla de ventas
*  Link del repositorio de la materia: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/
*  @author Jesus Said Llamas Manriquez
*  @version Estable v1
*/

class Practica11 extends Frame implements ActionListener{
   
   Button btnLibro, btnMochila, btnPluma, btnNVenta, btnFVenta, btnSalir;
   Label l, l1, l2, l3,l4;
   int lib = 0, pl = 0, mo = 0;
   int pLib = 120, pPl = 4, pMo = 830;
   
   /**
   *  Constructor vacio, me sirve para construir el frame y a�adir los elementos graficos
   */
   public Practica11(){
      setLayout(null);

      Label title = new Label("Papeleria");
      title.setFont(new Font("SanSerif", Font.BOLD, 26));
      title.setBounds(340,40,500,40);

      btnLibro = new Button("A�adir Libro");
      btnLibro.setBounds(30,110, 140, 23);
      btnLibro.addActionListener(this);

      btnMochila = new Button("A�adir Mochila");
      btnMochila.setBounds(180,110, 140, 23);
      btnMochila.addActionListener(this);

      btnPluma = new Button("A�adir Pluma");
      btnPluma.setBounds(330,110, 140, 23);
      btnPluma.addActionListener(this);

      btnNVenta = new Button("Venta nueva");
      btnNVenta.setBounds(480,110, 140, 23);
      btnNVenta.addActionListener(this);

      btnFVenta = new Button("Finalizar compra");
      btnFVenta.setBounds(630,110, 140, 23);
      btnFVenta.addActionListener(this);

      btnSalir = new Button("Salir");
      btnSalir.setBounds(780,110, 140, 23);
      btnSalir.addActionListener(this);

      l = new Label("0");
      l.setBounds(85,135, 40, 23);
      l1 = new Label("0");
      l1.setBounds(240,135, 40, 23);
      l2 = new Label("0");
      l2.setBounds(395,135,40, 23);
      l3 = new Label("# Piezas: ");
      l3.setBounds(530,135, 80, 23);
      l4 = new Label("$ Total: ");
      l4.setBounds(530,155, 80, 23);
      
      add(title);add(l);add(l1);add(l2);add(l3);add(l4);
      add(btnLibro);add(btnMochila);add(btnPluma);add(btnNVenta);add(btnFVenta);add(btnSalir);
   }
   
   /**
   *  Metodo para los eventos en los botones
   *   @param e ActionEvent para detectar el elemento el cual fue lanzador del evento   
   */
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == btnMochila){
         mo++;
         l1.setText(""+mo);
      }else if(e.getSource() == btnPluma){
         pl++;
         l2.setText(""+pl);
      }else if(e.getSource() == btnLibro){
         lib++;
         l.setText(""+lib);
      }else if(e.getSource() == btnFVenta){
         btnMochila.setEnabled(false);
         btnPluma.setEnabled(false);
         btnLibro.setEnabled(false);
         l3.setText("# Piezas: "+(mo+pl+lib));
         l4.setText("$ Total: "+(mo*pMo+lib*pLib+pl*pPl));
      }else if(e.getSource() == btnNVenta){
         btnMochila.setEnabled(true);
         btnPluma.setEnabled(true);
         btnLibro.setEnabled(true);
         mo = 0;pl = 0; lib = 0;
         l1.setText(""+mo);l2.setText(""+pl);l.setText(""+lib);
         l3.setText("# Piezas: ");
         l4.setText("$ Total: ");
      }else if(e.getSource() == btnSalir) System.exit(0);
   }
   
   /**
   *  Metodo principal. Me ayuda a correr la app
   *  @param args Array de parametros
   */
   public static void main(String[] args){
      Practica11 app = new Practica11();
      app.setBounds(360, 40, 950, 200); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tama�o
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }//main
}