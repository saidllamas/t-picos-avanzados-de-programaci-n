import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Practica16 extends Applet implements Runnable, ActionListener{

   int x = 25, y = 10;
   Thread t;
   Button play, stop;
   boolean ban = false;
   boolean abajo = true, derecha = true; //me ayuda a la orientacion incial
   
   public void init(){
      setLayout(null);
      play = new Button("Correr");
      play.setBounds(120, 30,80, 24);
      stop = new Button("Detener");
      stop.setBounds(210, 30,80, 24);
      
      play.addActionListener(this);
      stop.addActionListener(this);
      
      stop.setEnabled(false);
      
      add(stop);
      add(play);
   }
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == play){
         start();
         stop.setEnabled(true);
         play.setEnabled(false);
      }
      else{ t.stop();
         stop.setEnabled(false);
         play.setEnabled(true);
      }
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      while(true){
         if(y == 345) abajo = false;
         if(y == 10) abajo = true;
         if(x == 330) derecha = false;
         if(x == 25) derecha = true;
         
         if(abajo) y += 5;
         else y-= 5;
         
         if(derecha) x += 1;
         else x -= 1;
         repaint();
         pausa(10);
         
         //por si se sale del marco
         if(x > 500 | x < -500 | y > 500 | y < -500){ x = 10; y = 10;}
         
      }
   }
   
   public void paint(Graphics g){
      g.fillOval(x, y, 50, 50);
      g.fillRect(20,5, 360, 5);
      g.fillRect(20,394, 360, 5);
      g.fillRect(20, 8, 5, 388);
      g.fillRect(376, 6, 5, 391);
   }
   
   public static void main(String[] args){
      Frame f = new Frame();
      Practica16 p = new Practica16();
      p.init();
      f.add(p);
      f.setBounds(10,10,600,450);
      f.setVisible(true);
      f.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
            System.exit(0);
      }
     });
   }
}