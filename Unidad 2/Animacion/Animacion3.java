import java.awt.*;
import java.applet.*;

public class Animacion3 extends Applet implements Runnable{
   
   Thread t;
   boolean ban = false;
   int x = 10, y = 10, z= 10, v = 10;
   
   public void init(){
      
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      while(true){
         if(x < 300) x+= 10;
         else x = 10;
         repaint();
         pausa(200);
         if(ban){ break;}
      }
   }
   
   
   public void paint(Graphics g){
      g.setColor(Color.BLUE);
      g.fillOval(50+x,50,20+x,100);
      g.setColor(Color.BLACK);
      g.fillOval(50+x,150,20+x,20);
      g.drawLine(50,151,550,151);
   }
}