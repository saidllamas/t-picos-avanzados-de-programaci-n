import java.awt.*;
import java.applet.*;
import javax.swing.*;

public class Animacion5 extends Applet implements Runnable{
   
   Thread t;
   Image img;
   Image im[] = new Image[8];
   boolean ban = false;
   int x = 50, y = 50;
   
   public void init(){
      for(int i = 0; i < 8; i++){
         im[i] = new ImageIcon(""+(i+1)+".jpg").getImage();
      }
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      while(true){
         for(int i = 0; i < im.length; i++){
            img = im[i];
            x+=100;
            repaint();
            pausa(200);
         }
         x = 40;
         if(ban){ break;}
      }
   }
   
   
   public void paint(Graphics g){
      g.drawImage(img, x,y, this);
   }
}