import java.awt.*;
import java.applet.*;

public class PlantillaAnimacion extends Applet implements Runnable{
   Thread t;
   boolean ban = false;
   public void init(){
      
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      while(true){
         repaint();
         pausa(200);
         if(ban){ break;}
      }
   }
   
   
   public void paint(){
   }
}