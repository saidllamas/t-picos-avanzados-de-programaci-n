import java.awt.*;
import java.applet.*;

public class Animacion1 extends Applet implements Runnable{
   Thread t;
   boolean ban = false;
   
   int x = 10, y = 20;
   
   public void init(){
      
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      while(true){
         if(x > 300) x = 10;
         else x += 5;
         if(y > 300) y = 20;
         else y += 10;
         repaint();
         pausa(200);
         if(ban){ break;}
      }
   }
   
   
   public void paint(Graphics g){
      g.fillRect(x,y,50,50);
      g.fillOval(10,10,x,y);
   }
}