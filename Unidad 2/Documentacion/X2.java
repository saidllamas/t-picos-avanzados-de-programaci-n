import java.awt.*;
import javax.swing.JOptionPane;

/**
*  La clase X2 es un ejemplo <br>
*  sencillo que contiene cuatro <br>
*  metodos, dos atributos y <br>
*  una variable de clase
*
*  @author J. Said Llamas Manriquez
*  @version ver 2.0
*/

public class X2 extends JOptionPane{
   int a = 0;
   /**
   *  c es un atributo privado de la clase X2 de tipo char   
   */
   private char c = '0';
   static float d = 10.5f;
   
   public X2(int a, char c){
      this.a = a;
      this.c = c;
   }
   
   public static void main(){
      X2 obj = new X2(15, 'X');
      imprime(obj.a);
      imprime(obj.c);
      imprime(d);
   }
   
   /**
   *  El metodo <br>
   *  es de tipo privado y recibe un parametro y no retorna un valor   
   */
   private static void imprime(int a){
      showMessageDialog(null, "a: "+a);
   }
   
   public static void imprime(char c){
      showMessageDialog(null, "c: "+c);
   }
   
   public static void imprime(float d){
      showMessageDialog(null, "d: "+d);
   }
}