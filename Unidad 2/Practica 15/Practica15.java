
/**
*  <h1>Practica 15, ejercicio 1 </h1>
*  <p>Esta es la clase principal </p>
*/
public class Practica15 extends Thread{

   /** Metodo para correr el hilo*/
   public void run(){
      for(int i = 1; i <= 40; i++)
         if((i%4) == 0) System.out.println(i);
   }   
   
   /** @param  args Array de argumentos*/
   public static void main(String[] args){
      Practica15 app = new Practica15();
      app.start();
   }
}