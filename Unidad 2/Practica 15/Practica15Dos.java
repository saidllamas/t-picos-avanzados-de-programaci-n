import java.awt.*;
import java.applet.*;

public class Practica15Dos extends Applet implements Runnable{
   
   String msj = "Si no fuera por los excelentes profesores que he tenido hubiera sido un poco dif�cil para m� poder comprender la vida y a las personas tambi�n. Ellos me transmitieron conocimientos y tambi�n han sabido ser Padres y mejores amigos que nos han seguido durante a�os con sus ense�anzas y ayuda para crecer como grandes seres humanos. �Feliz d�a del Maestro!";
   Thread t;
   boolean ban = false;
   
   public void init(){
      
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      for(int i = 0; i < msj.length(); i++) System.out.println(msj.charAt(i));
   }
}