public class Practica15Cuatro{
   public static void main(String[] args){
      Practica15 p1 = new Practica15();
      Practica15 p1copia = new Practica15();
      p1.start();
      p1copia.start();
      
      Practica15Dos p2 = new Practica15Dos();
      Practica15Dos p2copia = new Practica15Dos();
      p2.start();
      p2copia.start();
      
      Practica15Tres p3 = new Practica15Tres();
      p3.start();
      
   }
}