import java.awt.*;
import java.applet.*;

public class Practica15Tres extends Applet implements Runnable{
   
   Thread t;
   boolean ban = false;
   
   public void init(){
      
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      int random = (int)Math.floor(Math.random()*40);
      for(int i = 1; i <= random; i++) System.out.print("_ ");
   }
}