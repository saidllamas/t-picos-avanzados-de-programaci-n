package principal;

import principal.hilo1.*;
import principal.hilo2.*;
import principal.hilo3.*;

/**
*  <h1>Practica 15 </h1> <br>
*  <p>Esta es la clase principal del programa. Inicia tres hilos diferentes </p>
*  @author Jesus Said Llamas Manriquez
*  @version Estable v1
*/
public class Practica15Cuatro{

   /**
   *Metodo principal para inciar la aplicacion
   *@param  args Array de argumentos por default*/
   public static void main(String[] args){
      Practica15 p1 = new Practica15();
      Practica15 p1copia = new Practica15();
      p1.start();
      p1copia.start();
      
      Practica15Dos p2 = new Practica15Dos();
      Practica15Dos p2copia = new Practica15Dos();
      p2.start();
      p2copia.start();
      
      Practica15Tres p3 = new Practica15Tres();
      p3.start();
      
   }
}