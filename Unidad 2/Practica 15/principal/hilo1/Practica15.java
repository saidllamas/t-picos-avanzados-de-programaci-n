package principal.hilo1;

/**
*  <h1>Practica 15, ejercicio 1 </h1>
*  <p>Imprime los numeros divisbles entre 4 con un rango de 1 a 40 </p>
*/
public class Practica15 extends Thread{

   /** Metodo para correr el hilo*/
   public void run(){
      for(int i = 1; i <= 40; i++)
         if((i%4) == 0) System.out.println(i);
   }   
   
   /** @param  args Array de argumentos*/
   public static void main(String[] args){
      Practica15 app = new Practica15();
      app.start();
   }
}