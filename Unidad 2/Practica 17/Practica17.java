import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import javax.swing.*;

public class Practica17 extends Applet implements Runnable{
   
   Thread t;
   Image img, nube,nube2, fondo;
   Image im[] = new Image[8];
   boolean ban = false;
   int x = 50, y = 380, nX = 340, nY = 20, nY2 = 80, nX2 = 760;
   
   public void init(){
      for(int i = 0; i < 8; i++){
         im[i] = new ImageIcon(""+(i+1)+".png").getImage();
      }
      nube = new ImageIcon("nube.png").getImage();
      nube2 = new ImageIcon("nube.png").getImage();
      fondo = new ImageIcon("fondo.png").getImage();
      start();
   }
   
   public void start(){
      t = new Thread(this);
      ban = false;
      t.start();
   }
   
   public void stop(){
      ban = true;
      t = null;
   }
   
   public void pausa(int tiempo){
      try{
         Thread.sleep(tiempo);
      }catch(InterruptedException e){}
   }
   
   public void run(){
      while(true){
         for(int i = 0; i < im.length; i++){
            img = im[i];
            x += 100;
            nX -= 33;
            nX2 -= 14;
            nY  -= 4;
            nY2 -= 5;
            repaint();
            pausa(200);
            nY+= 6;
         }
         nX = 340;
         nY = 20;
         nY2 = 26;
         nX2 = 780 ;
         x = 40;
         if(ban){ break;}
      }
   }
   
   
   public void paint(Graphics g){
      g.drawImage(fondo,0, 0, this );
      g.drawImage(img, x,y, this);
      g.drawImage(nube,nX, nY, this );
      g.drawImage(nube2,nX2, nY2, this );
   }
   
   public static void main(String[] args){
      Frame f = new Frame();
      Practica17 p = new Practica17();
      p.init();
      f.add(p);
      f.setBounds(10,10,600,450);
      f.setVisible(true);
      f.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
            System.exit(0);
      }
     });
   }
}