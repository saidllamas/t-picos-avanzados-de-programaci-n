package principal;

import javax.swing.*;

import principal.asteriscos.*;
import principal.cuadrado.*;
import principal.mensaje.*;

/**
*  <h1>Practica 12 </h1> <br>
*  <p>Esta es la clase principal del programa. </p>
*  @author Jesus Said Llamas Manriquez
*  @version Estable v1
*/
class Principal{
   
   /**Constructor por defecto, hace instancias de cada clase para ejecutar las operaciones correspondientes*/
   public Principal(){
   		while(true){
   			int seleccion = JOptionPane.showOptionDialog(null,"Seleccione una clase","Selector de opciones",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null, new Object[] { "Cuadrado", "Mensaje", "Asteriscos", "Salir" },  "");
   			switch (seleccion) {
   				case 0:
		      		new Cuadrado();
		      	break;
		      	case 1:
                  new Mensaje();
		      	break;
		      	case 2:
		      		new Asteriscos();
		      	break;
		      	case 3:
		      		System.exit(0);
		      	break;
		      }
   		}
   }
   
   /** @param  args Array de argumentos*/
   public static void main(String[] args){
      new Principal();
   }
}