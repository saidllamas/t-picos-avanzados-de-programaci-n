package principal.mensaje;

import javax.swing.*;

/**
*  <p>Esta clase que pertenece al paquete principal.mensaje.
*  contendr� en una cadena un mensaje a mostrar, dicha cadena va a ser de tipo private.
*  Tambi�n esta clase tendr� un m�todo que ser� el que mostrar� el mensaje, para ello utilizar un cuadro de dialogo.
*  Como extra, decidi sobrecargar el metodo pasandole como parametro una cadena personalizable.</p>
*  @see Principal
*/

public class Mensaje{
   //Cadena pre-definida a imprimir
   private String cadena = "Saludos Maestra.";
   
   /**
   *  Constructor vacio, por defecto, ejecuto tanto los metodos para imprimir una cadena
   */
   public Mensaje(){
      printMensaje();
      String msj = JOptionPane.showInputDialog("Ingresar mensaje: ");
      printMensaje(msj);
   }
   
   /**
   *  Sobrecarga del metodo para enviar una cadena cual sea y se imprima
   *  @param msj Recibe una cadena cual sea y la imprime por medio de un JOptionPane
   */
   public void printMensaje(String msj){
      JOptionPane.showMessageDialog(null, msj);
   }
   
   /**
   *  Metodo original, imprime una variable global marcada como privada
   */
   public void printMensaje(){
      JOptionPane.showMessageDialog(null, cadena);
   }
}