package principal.cuadrado;

import javax.swing.*;

/**
*  clase que pertece al paquete principal.cuadrado, ofrece un m�todo para obtener el cuadrado de un n�mero,
*  el cual se enviar� como par�metro, en el mismo m�todo getNumero 
*  resultado imprimiendo el n�mero original y su cuadrado, para ello se utiliza un cuadro de dialogo.
*/

public class Cuadrado{

   /**
   *  Constructor vacio, por defecto, ejecuto el metodo para obtener e imprimir el cuadrado del numero y el numero original
   */
   public Cuadrado(){
      getNumero();
   }
   
   /**
   *  No recibe parametros, a traves de un JOptionPane se solicita un numero y se imprime igualmente con un JOptionPane
   */
   public void getNumero(){
      int num = 0;
      try{
         num = Integer.parseInt(JOptionPane.showInputDialog("Ingresar mensaje: "));
      }catch(Exception e){}
      JOptionPane.showMessageDialog(null, "Original: "+num+"\nCuadrado: "+num*num);
   }
}