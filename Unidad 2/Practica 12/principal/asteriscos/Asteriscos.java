package principal.asteriscos;

import javax.swing.*;

/**
*  Clase que pertenece al paquete principal.asteriscos, en la cual se tiene un m�todo donde
*  se pedir� un valor entero y de acuerdo a �ste, se imprime un cuadrado de asteriscos con la
*  cantidad proporcionada, por ejemplo si el valor que se captur� fue 5, se imprimir� un
*  cuadrado de 5 renglones y 5 columnas de asteriscos.
*/
public class Asteriscos{
   
   /**
   *  Constructor vacio, por defecto, ejecuto el metodo solicitando el numero e imprimiendo la cascada de *
   */
   public Asteriscos(){
      getNumero();
   }
   
   /**
   *  No recibe parametros, El metodo solicita un numero entero , lo imprime en la consola y en un JOptionPane
   */
   public void getNumero(){
      int numero = 0;
      try{
         numero = Integer.parseInt(JOptionPane.showInputDialog("Ingresar numero: "));
      }catch(Exception e){}
      String chars = "";
      for(int i = 1; i <= numero; i++){
         for(int j = 1; j < numero; j++){
            System.out.print(" * ");
            chars += " * ";
         }
         System.out.println(" * ");
         chars += " * \n";
      }
      JOptionPane.showMessageDialog(null, chars);
   }
}