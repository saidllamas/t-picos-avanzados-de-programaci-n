public class X{
   public static void main(String[] args){
      Thread h1 = new Thread();
      Thread h2 = new Thread("Hilo 2");
      
      h2.setDaemon(true);
      
      h1.start();
      h2.start();
      
      Thread x = Thread.currentThread();
      x.setPriority(10);
      x.setName("Principal");
      System.out.println(Thread.currentThread().getName());
      System.out.println(Thread.currentThread().getPriority());
      System.out.println(Thread.currentThread().isDaemon());
      
      System.out.println(h1.getName()+" "+h2.getName());
      System.out.println(h1.getPriority()+" "+h2.getPriority());
      System.out.println("       "+h2.isDaemon());
      System.out.println(h1.isAlive()+" "+h2.isAlive());
   }
}