import java.awt.*;
import java.applet.*;

public class Cuadrito extends Applet{
	public void init(){
		setBackground(Color.white);
	}

	public void paint(Graphics g){
		Font fuente = new Font("SanSerif", Font.BOLD, 20);
		g.setFont(fuente);
		for (int y = 50; y <= 250; y+=10) { //izq-der
			for (int i = 10; i <= 210; i+=10) {
				g.drawString("*", i, y);
				g.setColor(new Color((float)Math.random(), (float)Math.random(), (float)Math.random()));
				try{
					Thread.sleep(0);
				} catch(InterruptedException e){}
			}//--for
		}//for
		
		for (int y = 10; y <= 210; y+=10) { //columna arriba - abajo
			for (int i = 350; i <= 550; i+=10) {
				g.drawString("*", y, i);
				g.setColor(new Color((float)Math.random(), (float)Math.random(), (float)Math.random()));
				try{
					Thread.sleep(0);
				} catch(InterruptedException e){}
			}//--for
		}//for
		
		int x = 230;
		for (int y = 50; y <= 250; y += 10) {
			g.drawString("*", x+=20, y);
			g.setColor(new Color((float)Math.random(), (float)Math.random(), (float)Math.random()));
			try{
				Thread.sleep(50);
			} catch(InterruptedException e){}
		}//for
	}//paint
}//class