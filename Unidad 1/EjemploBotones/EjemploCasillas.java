import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class EjemploCasillas extends Applet implements ItemListener{
   Checkbox c1, c2, c3, c4, c5, c6;
   String cad = "";
   public void init(){
      
      c1 = new Checkbox("Casilla 1");
      c2 = new Checkbox("Casilla 2");
      c3 = new Checkbox("Casilla 3");
      c4 = new Checkbox("Casilla 4");
      c5 = new Checkbox("Casilla 5");
      c6 = new Checkbox("Casilla 6");
      
      c4.setBackground(Color.ORANGE);
      c5.setBackground(Color.ORANGE);
      c6.setBackground(Color.ORANGE);
           
      
      add(c1);
      add(c2);
      add(c3);
      add(c4);
      add(c5);
      add(c6);
      
      c1.addItemListener(this);
      c2.addItemListener(this);
      c3.addItemListener(this);
      c4.addItemListener(this);
      c5.addItemListener(this);
      c6.addItemListener(this);      
   }
   
   public void itemStateChanged(ItemEvent e){
      cad = (String)e.getItem();
      if(e.getStateChange() == ItemEvent.SELECTED) cad += " ha sido seleccionado";
      else cad += " se quito la seleccion";
      
      if(e.getItem() == "Casilla 4"){
         if(c4.getState()) c4.setBackground(Color.YELLOW);
         else c4.setBackground(Color.ORANGE);
      }else if(e.getItem() == "Casilla 5"){
         if(c5.getState()) c5.setBackground(Color.GRAY);
         else c5.setBackground(Color.ORANGE);
      }else if(e.getItem() == "Casilla 6"){
         if(c6.getState()) c6.setBackground(Color.MAGENTA);
         else c6.setBackground(Color.ORANGE);
      }
      repaint();
   }
   
   public void paint(Graphics g){
      g.drawString(cad, 100, 100);
   }
   
}