import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class EjemploListaDesplegable extends Applet implements ItemListener{

   Choice lista;
   
   public void init(){
      lista = new Choice();
      lista.add("Cian");
      lista.add("Rosa");
      lista.add("Morado");
      
      add(lista);
      add(new Label("Cambio de color"));
      
      lista.addItemListener(this);
   }
   
   public void itemStateChanged(ItemEvent e){
      if(lista.getSelectedItem().equals("Cian")){
         setBackground(Color.CYAN);
      }else if(lista.getSelectedIndex() == 1){
         setBackground(Color.PINK);
      }else if(lista.getSelectedItem().equals("Morado")){
         setBackground(new Color(100,0,100));
      }
      repaint();
   }
   
   public void paint(Graphics g){
   }
   
}