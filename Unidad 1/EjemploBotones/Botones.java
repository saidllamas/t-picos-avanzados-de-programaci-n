import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Botones extends Panel implements ActionListener{
   Button b, b2, b3, b4, b5, b6, b7;
   int ID = 0;
   
   public Botones(){
      b = new Button("boton 1");
      b2 = new Button("boton 2");
      b3 = new Button("Boton 3");
      b.addActionListener(this);
      b2.addActionListener(this);
      b3.addActionListener(this);
      b.setBounds(100, 20, 50, 20);
      b2.setBounds(100, 50, 50, 20);
      b3.setBounds(100, 80, 50, 20);
      add(b);
      add(b2);
      add(b3);
      //cuatro botones mas
      b4 = new Button("Cuadrado");
      b5 = new Button("Ovalo");
      b6 = new Button("Triangulo");
      b7 = new Button("Cruz");
      b4.addActionListener(this);
      b5.addActionListener(this);
      b6.addActionListener(this);
      b7.addActionListener(this);
      b4.setBounds(170, 20, 50, 20);
      b5.setBounds(170, 50, 50, 20);
      b6.setBounds(170, 80, 50, 20);
      b7.setBounds(170, 110, 50, 20);
      add(b4);
      add(b5);
      add(b6);
      add(b7);
      setLayout(null);
   }
   
   public void paint(Graphics g){
      if(ID == 1){
         g.drawRect(110,110,30, 30); //CUADRADO
      }else if(ID == 2){
         g.drawOval(110, 120, 30, 30); //OVALO
      }else if(ID == 3){
         int xT[] = {110, 120, 130, 110};
   		int yT[] = {200, 180, 200, 200};
   		g.drawPolyline(xT, yT, 4);
      }else if(ID == 4){
         //g.drawLine(110,210, 100, 100); //Diagonal
   		g.drawLine(150,150, 200, 200); //Diagonal
         g.drawLine(150,200, 200, 150); //Diagonal
      }
   }
   
   public static void main(String[] args){
      Frame f = new Frame();
      //f.setLayout(null);
      
		f.setBounds(10,10,600,400);
	   f.add(new Botones());
		f.setVisible(true);
      f.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
      
   }
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == b3){
         System.out.println("b3");
         Color generado = new Color((float)Math.random(), (float)Math.random(), (float)Math.random(), (float)Math.random());
         setBackground(generado);
         b3.setBackground(generado);
         b2.setBackground(generado);
         b.setBackground(generado);
      }else if(e.getSource() == b){
         setBackground(Color.BLUE);
         b.setBackground(Color.BLUE);
         System.out.println("b");
      }else if(e.getSource() == b2){
         System.out.println("b2");
         setBackground(Color.YELLOW);
         b2.setBackground(Color.YELLOW);
      }else{
         if(e.getSource() == b4){
            ID = 1;
         }else if(e.getSource() == b5){
            ID = 2;
         }else if(e.getSource() == b6){
            ID = 3;
         }else if(e.getSource() == b7){
            ID = 4;
         }
         repaint();
      }
   }//end action performed
   
}//class