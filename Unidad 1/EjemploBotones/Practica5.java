import java.awt.*;
import java.awt.event.*;

/*
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: El siguiente programa genera una ventana con 8 botones y cuatro barras
*  de desplazamiento con lo cual manipula un figura (cuadrada) en tiempo de ejecucion.
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src/51389e27115291004d987c5327f02fc80c905ef1/EjemploBotones/Practica5.java?at=master&fileviewer=file-view-default
*/

public class Practica5 extends Frame implements AdjustmentListener, ActionListener{
   
   Scrollbar r, g, b, trans; //barras 
   Button b_1, b_2, b_3, b_4, b_5, b_6, b_7, b_8; //botones
   
   int x, y, x2, y2; //valores en las barras
   
   Color fondo, cBorder;
   
   boolean border; //pintar borde?
   
   int inX = 560, inY = 200, posX = 50, posY = 50; //valores del rectangulo a manipular
   
   public Practica5(){
      setLayout(null);
      
      b_1 = new Button("Izquierda");
      b_2 = new Button("Derecha");
      b_3 = new Button("Arriba");
      b_4 = new Button("Abajo");
      b_5 = new Button("Contorno");
      b_6 = new Button("Relleno");
      b_7 = new Button("Agrandar");
      b_8 = new Button("Disminuir");
      
      b_1.addActionListener(this);
      b_2.addActionListener(this);
      b_3.addActionListener(this);
      b_4.addActionListener(this);
      b_5.addActionListener(this);
      b_6.addActionListener(this);
      b_7.addActionListener(this);
      b_8.addActionListener(this);
      
      b_1.setBounds(20, 60,60,40);
      b_2.setBounds(20,105,60,40);
      b_3.setBounds(20,150,60,40);
      b_4.setBounds(20,195,60,40);
      b_5.setBounds(20,240,60,40);
      b_6.setBounds(20,285,60,40);
      b_7.setBounds(20,330,60,40);
      b_8.setBounds(20,375,60,40);
      
      add(b_1);
      add(b_2);
      add(b_3);
      add(b_4);
      add(b_5);
      add(b_6);
      add(b_7);
      add(b_8);
      
      r = new Scrollbar(Scrollbar.VERTICAL, 0, 1,0, 255);
      g= new Scrollbar(Scrollbar.VERTICAL, 0, 1,0, 255);
      b = new Scrollbar(Scrollbar.VERTICAL, 0, 1,0, 255);
      trans = new Scrollbar(Scrollbar.VERTICAL, 0, 1,0, 255);
      
      r.setBounds(90,60,60,350);
      g.setBounds(160,60,60,350);
      b.setBounds(230,60,60,350);
      trans.setBounds(300,60,60,350);
      
      add(r);
      add(g);
      add(b);
      add(trans);
      
      r.addAdjustmentListener(this);
      g.addAdjustmentListener(this);
      b.addAdjustmentListener(this);
      trans.addAdjustmentListener(this);
   }
   
   public void adjustmentValueChanged(AdjustmentEvent e){
      x = r.getValue();
      y = g.getValue();
      x2 = b.getValue();
      y2 = trans.getValue();
      fondo = new Color(x,y,x2,y2);
      repaint();
   }
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == b_1){
         inX -= 5;
      }else if(e.getSource() == b_2){
         inX += 5;
      }else if(e.getSource() == b_3){
         inY -= 5;
      }else if(e.getSource() == b_4){
         inY += 5;
      }else if(e.getSource() == b_5){
         cBorder = new Color((int)((float)Math.random()*255), (int)((float)Math.random()*255), (int)((float)Math.random()*255));
         if(border) border = false;
         else border = true;
      }else if(e.getSource() == b_6){
         fondo = new Color((int)((float)Math.random()*255), (int)((float)Math.random()*255), (int)((float)Math.random()*255), (int)((float)Math.random()*255));
      }else if(e.getSource() == b_7){
         posX += 5;
         posY += 5;
      }else if(e.getSource() == b_8){
         posX -= 5;
         posY -= 5;
      }
      repaint();
   }//end action performed
   
   public void paint(Graphics g){
      g.drawRect(380, 60, 385, 385); //area de dibujo
      if(border){
         g.setColor(cBorder); //cambio el color al del borde
         g.drawRect(inX, inY, posX, posY); //pinto el borde
         g.setColor(fondo);//cambio color al fondo
         g.fillRect(inX, inY, posX, posY); //relleno figura
      }else{
         g.setColor(fondo);
         g.fillRect(inX, inY, posX, posY);
      }
      g.setColor(Color.BLACK);
      g.drawString("Red: "+x, 90, 435);
      g.drawString("Green: "+y, 160, 435);
      g.drawString("Blue: "+x2, 230, 435);
      g.drawString("Alpha: "+y2, 300, 435);
      g.drawString("Color Fondo: "+fondo, 475, 425);
      g.drawString("Color Borde: "+cBorder, 475, 440);
      g.drawString("J. Said Llamas Manriquez", 120, 510);//info
      g.drawString("Cuando se presione el boton 'Contorno' se habilita el contorno y se genera un color,", 50, 545);//info
      g.drawString("el contorno se quedara hasta no presionar de nuevo el mismo boton de contorno.", 50, 565);//info
   }
   
   public static void main(String[] args){
      Practica5 f = new Practica5();
      f.setBounds(0,0,800,600);
      f.setVisible(true);     
      f.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });

   }
}