import java.awt.*;
import java.awt.event.*;

public class EjemBarras extends Frame implements AdjustmentListener{
   Scrollbar b1, b2, b3, b4;
   int x, y, x2, y2;
   public EjemBarras(){
      b1 = new Scrollbar(Scrollbar.HORIZONTAL);
      b2 = new Scrollbar(Scrollbar.HORIZONTAL);
      b3 = new Scrollbar();
      b4 = new Scrollbar();
      add(b1, "North");
      add(b2, "South");
      add(b3, "West");//izq
      add(b4, "East");//der
      b1.addAdjustmentListener(this);
      b2.addAdjustmentListener(this);
      b3.addAdjustmentListener(this);
      b4.addAdjustmentListener(this);
   }
   
   public void adjustmentValueChanged(AdjustmentEvent e){
      x = b1.getValue();
      y = b2.getValue();
      x2 = b3.getValue();
      y2 = b4.getValue();
      repaint();
   }
   
   public void paint(Graphics g){
      g.drawString("Barra 1: "+x, 100, 100);
      g.drawString("Barra 2: "+y, 100, 140);
      g.drawString("Barra 3: "+x2, 100, 180);
      g.drawString("Barra 4: "+y2, 100, 220);
   }
   
   public static void main(String[] args){
      EjemBarras f = new EjemBarras();
      f.setBounds(0,0,800,600);
      f.setVisible(true);     
      f.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });

   }
}