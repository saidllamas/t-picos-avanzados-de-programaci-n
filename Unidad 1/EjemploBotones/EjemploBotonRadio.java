import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class EjemploBotonRadio extends Applet implements ItemListener{

   Checkbox b1, b2, b3;
   Checkbox b4, b5, b6, b7, b8;
   int align;
   
   public void init(){
      CheckboxGroup grupo1 = new CheckboxGroup();
      b1 = new Checkbox("Boton 1", false, grupo1);
      b2 = new Checkbox("Boton 2", false, grupo1);
      b3 = new Checkbox("Boton 3", false, grupo1);
      b4 = new Checkbox("Izquierda", false, grupo1);
      b5 = new Checkbox("Derecha", false, grupo1);
      b6 = new Checkbox("Arriba", false, grupo1);
      b7 = new Checkbox("Abajo", false, grupo1);
      b8 = new Checkbox("Centro", false, grupo1);
      
      add(b1);
      add(b2);
      add(b3);
      add(b4);
      add(b5);
      add(b6);
      add(b7);
      add(b8);
      
      b1.addItemListener(this);
      b2.addItemListener(this);
      b3.addItemListener(this);
      b4.addItemListener(this);
      b5.addItemListener(this);
      b6.addItemListener(this);      
      b7.addItemListener(this);
      b8.addItemListener(this);
   }
   
   public void itemStateChanged(ItemEvent e){
      if(b1.getState()){
         setBackground(Color.CYAN);
      }else if(b2.getState()){
         setBackground(Color.PINK);
      }else if(b3.getState()){
         setBackground(Color.BLUE);
      }else if(b4.getState()){
         align = 1;
      }else if(b5.getState()){
         align = 2;
      }else if(b6.getState()){
         align = 3;
      }else if(b7.getState()){
         align = 4;
      }else if(b8.getState()){
         align = 5;
      }else{
         setBackground(Color.WHITE);
      }
      repaint();
   }
   
   public void paint(Graphics g){
      g.setColor(Color.RED);
      switch(align){
         case 1:
            g.fillOval(50,200, 60,60);
         break;
         case 2: 
            g.fillOval(350,200, 60,60);
         break;
         case 3: 
            g.fillOval(200,50, 60,60);
         break;
         case 4: 
            g.fillOval(200,350, 60,60);
         break;
         case 5: 
            g.fillOval(200,200, 60,60);
         break;
         default: 
            g.fillOval(200,200, 60,60);
         break;
      }
   }
   
}