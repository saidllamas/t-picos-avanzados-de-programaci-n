import java.applet.*;
import java.awt.*;
import javax.swing.*;


public class Main extends Applet{
	Image im;
	public void init(){
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		//ImageIcon ii = new ImageIcon("archivo.txt");
		//im = ii.getImage
		im = getImage(getCodeBase(), "carro.png");
		setBackground(new Color(255, 127, 39, 100));
		resize((int)d.getWidth(), (int)d.getHeight());
	}//end init

	public void paint(Graphics g){
		g.drawImage(im, 10, 10, 90, 140, 130, 170, 30, 30, new Color(34,34,34), this);
	}//end paint
}//end class