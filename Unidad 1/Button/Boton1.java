import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Boton1 extends Applet implements ActionListener{
   public void init(){
      Button b = new Button("Prueba 1");
      add(b);
      b.addActionListener(this);
   }
   public void actionPerformed(ActionEvent e){
      setBackground(new Color((float)Math.random(), .5f, .5f, .5f));
   }
}