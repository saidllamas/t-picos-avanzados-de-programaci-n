import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Boton2 extends Applet{
   public void init(){
      Button b = new Button("Prueba 2");
      add(b);
      b.addActionListener(new ActionListener(){
         public void actionPerformed(ActionEvent e){
            setBackground(new Color((float)Math.random(), .5f, .5f, .5f));
         }
      });
   }
}