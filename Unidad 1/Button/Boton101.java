import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Boton101 extends Applet implements ActionListener{
   Button b, b2;
   public void init(){
      b = new Button("Prueba 2");
      b2 = new Button("Prueba 2");
      add(b);
      add(b2);
      b.addActionListener(this);
      b2.addActionListener(this);
   }
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == b){
         System.out.println("b");
         b.setBackground(new Color(.1f, (float)Math.random(), .1f, .1f));
      }else{
         System.out.println("b2");
         b2.setBackground(new Color(.9f, (float)Math.random(), .9f, .9f));
      }
   }
}