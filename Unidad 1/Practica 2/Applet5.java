import java.applet.*;
import java.awt.*;

public class Applet5 extends Applet{
	public void init(){
		resize(450,450);
	}//init
	public void paint(Graphics g){
      g.drawOval(130, 180, 175, 200); //contorno cabeza 
		g.drawOval(170, 230, 20, 20); //ojo izquierdo
		g.fillOval(172, 238, 16, 14); //pupila izquierdo
		g.drawOval(245, 230, 20, 20); //ojo derecho
		g.fillOval(247, 238, 16, 14); //pupila derecha
		g.drawOval(197, 270, 48, 52);//boca
		g.drawOval(173, 320, 15, 32);//gota
	}//paint
}//class