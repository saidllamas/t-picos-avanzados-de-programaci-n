import java.applet.*;
import java.awt.*;

public class Applet1 extends Applet{
	static int[][] arreglo1 = new int[10][5];
	public void init(){
		resize(250,250);
	}//init
	public void paint(Graphics g){
		//Arreglo 1 10x5
		int c = 1;
		for(int i = 0; i < 10; i++){
			if((i % 2) == 0){
				for (int x = 0; x < 5; x++) {
					arreglo1[i][x] = c++;
				}//--for
			}//if
		}//for
		
		for (int i = 0; i < 10; i++) {
			for (int x = 0; x < 5; x++) {
				if(arreglo1[i][x] != 0)	g.drawString("*", (x*15), (i*15)); //verifico que tenga numero real
				//else g.drawString("-", (x*15), (i*15)); //para diferenciar los espacios
			}//--for
		}//for
		

	}//paint
}//class