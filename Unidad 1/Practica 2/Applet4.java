import java.applet.*;
import java.awt.*;

public class Applet4 extends Applet{
	static int[][] arreglo4 = new int[10][10];
	public void init(){
		resize(250,250);
	}//init
	public void paint(Graphics g){
		int c = 1;
		for (int i = 0; i < 10; i++) {
			for (int x = i; x < 10 - i; x++) {	//izq-der
                arreglo4[i][x] = c++;
            }//for
            for (int x = i; x < 10 - i; x++) {	//arr-ab
                arreglo4[x][9 - i] = c++;
            }//for
            for (int x = 9 - i; x > i ; x--) {  //der-izq
                arreglo4[9 - i][x] = c++;
            }//for
            for (int x = 9 - i; x > i; x--) {	//aba-arr
               	arreglo4[x][i] = c++;
            }//for
		}//for
      for (int i = 0; i < 10; i++) {
			for (int x = 0; x < 10; x++) {
				if(arreglo4[i][x] != 0)	g.drawString("*", (x*15), (i*15)); //verifico que tenga numero real
			}//--for
		}//for
	}//paint
}//class