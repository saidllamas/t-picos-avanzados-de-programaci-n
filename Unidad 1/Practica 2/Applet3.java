import java.applet.*;
import java.awt.*;

public class Applet3 extends Applet{
	static int[][] arreglo3 = new int[10][8];
	public void init(){
		resize(250,250);
	}//init
	public void paint(Graphics g){
		int c = 1;
		boolean haciaDerecha = true; //de izq a der
		for (int w = 0; w < 10; w+=2) { //Renglones a saltar
			if(haciaDerecha){
				for (int i = 0;  i < 8; i++) {
					arreglo3[w][i] = c++;
					g.drawString("*", (i*15), (w*15)); //verifico que tenga numero realb
               /*
					try{
						Thread.sleep(50);
					} catch(InterruptedException e){}
               */
				}
				haciaDerecha = false; //cambia el signo
			}else{
				for (int i = 7;  i >= 0; i--) {
					arreglo3[w][i] = c++;
					g.drawString("*", (i*15), (w*15)); //verifico que tenga numero realb
               /*
					try{
						Thread.sleep(50);
					} catch(InterruptedException e){}
               */
				}
				haciaDerecha = true; //cambia el signo
			}//else
		}//for
	}//paint
}//class