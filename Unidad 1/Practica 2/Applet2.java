import java.applet.*;
import java.awt.*;

public class Applet2 extends Applet{
	static int[][] arreglo2 = new int[5][10];
	public void init(){
		resize(250,250);
	}//init
	public void paint(Graphics g){
		//Arreglo 2 5x10
		int c = 1; //reinicio el contador
		for (int i = 0; i < 10; i++) {
			if((i % 2) != 0){
				for (int x = 0; x < 5 ; x++) {
					arreglo2[x][i] = c++;
				}//--for
			}//if
		}//for
		
		for (int i = 0; i < 5; i++) {
			for (int x = 0; x < 10; x++) {
				if(arreglo2[i][x] != 0)	g.drawString("*", (x*15), (i*15)); //verifico que tenga numero real
				try{
					Thread.sleep(50);
				} catch(InterruptedException e){}
				//else g.drawString("-", (x*15), (i*15)); //para diferenciar los espacios
			}//--for
		}//for
		

	}//paint
}//class