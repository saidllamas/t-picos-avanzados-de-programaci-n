//Ejemplo GridBagLayout
import java.awt.*;
import java.awt.event.*;

//Ejemplo 5 del uso del GriBagLayout

public class Layout10 extends Panel
{public Layout10()
 {
  GridBagLayout gb=new GridBagLayout();
  GridBagConstraints construye=new GridBagConstraints();
  setLayout(gb);
  construye.gridx=0;
  construye.gridy=0;
  add(new Button("uno"),construye);
  // ipadx e ipady permiten establecer un tama�o especifico para el componente
  construye.ipadx=25;
  construye.ipady=75;
  construye.gridx=1;
  construye.gridy=0;
  add(new Button("dos"),construye);
  construye.gridx=1;
  construye.gridy=1;
  add(new Button("tres"),construye);
  construye.ipadx=0;
  construye.ipady=0;
  construye.gridx=2;
  construye.gridy=1;
  add(new Button("cuatro"),construye);
  construye.gridx=0;
  construye.gridy=2;
  add(new Button("cinco"),construye);
  
 }
 
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout10());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

