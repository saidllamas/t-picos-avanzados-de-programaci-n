//ejemplo BorderLayout
import java.awt.*;
import java.applet.*;
public class pruebaBorderLayout extends Applet
{public void init()
 {//setLayout(new BorderLayout());
  setLayout(new BorderLayout(20,20));
  add(BorderLayout.NORTH,new Button("Boton NORTH"));
  add(new Button("Boton WEST"),BorderLayout.WEST);
  add("Center",new Button("Boton Center"));
  add(new Button("Boton EAST"),"East");
  add("South",new Button("Boton SOUTH"));
  }
}


