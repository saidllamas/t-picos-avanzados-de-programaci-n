//Ejemplo CardLayout
import java.awt.*;
import java.awt.event.*;

public class Layout11 extends Panel
{CardLayout cl=new CardLayout();
 Panel tarjetas=new Panel();
 Panel botones=new Panel();
 public Layout11()
 {Button b1;
  setLayout(new BorderLayout());
  botones.add(b1=new Button("->Siguiente<-"));
  b1.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.next(tarjetas);}});  
  add(botones,"South");
  tarjetas.setLayout(cl);
  tarjetas.add(new Button("Card 1"),"Tarjeta 1");
  tarjetas.add(new Button("Card 2"),"Tarjeta 2");
  tarjetas.add(new Button("Card 3"),"Tarjeta 3");  
  add(tarjetas,"Center");
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout11());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

