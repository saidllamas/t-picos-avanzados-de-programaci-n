import java.awt.*;
import java.applet.*;
import java.awt.event.*;

public class Linea extends Applet implements MouseListener, MouseMotionListener
{int x1=0, y1=0;
 int x2=0, y2=0;
 boolean draw;

 public void init()  // si se tratara de una clase que hereda de clase Frame y no de Applet, �ste ser�a el m�todo constructor
 {addMouseListener(this);
  addMouseMotionListener(this);
  draw=false;
 }

// los m�todos que vienen a continuaci�n, quedan con el cuerpo vac�o ya que no se utilizaran en esta aplicaci�n, pero como
// indicamos en la clase que se va a implementar las interfaces MouseListener y MouseMotionListener, es necesario 
// implementar los m�todos correspondientes a cada interface, se necesiten o no en la aplicaci�n, si no se necesitan quedan
// con el cuerpo vac�o como los casos siguientes:

// m�todos de la interface MouseListener
 
 public void mouseEntered(MouseEvent e){}

 public void mouseExited(MouseEvent e){}

 public void mouseClicked(MouseEvent e){}

 public void mousePressed(MouseEvent e)
 {x2=e.getX();
  y2=e.getY();
 draw=true;
 }

 public void mouseReleased(MouseEvent e)
 {draw=false;}

// interface MouseMotionListener, �sta contiene solo dos m�todos, los cuales si se utilizar�n en este ejemplo, por lo tanto 
// el cuerpo no estar� vac�o

 public void mouseDragged(MouseEvent e)
 {x1=e.getX();
  y1=e.getY();
  repaint();
}

 public void mouseMoved(MouseEvent e)
 {showStatus("Moviendo mouse en: "+e.getX()+","+e.getY());}

 public void paint(Graphics g)
 {if (draw) {g.drawLine(x1,y1,x2,y2);}
 }


}
