import java.awt.*;
import java.applet.*;
import java.awt.event.*;
public class LineaVer2 extends Applet implements MouseMotionListener
{int x1=0, y1=0;
 int x2=0, y2=0;
 boolean draw;

 public void init()   
 {addMouseListener(new MouseAdapter() // se implementan los m�todos de la clase abstracta MouseAdapter
                                  { public void mousePressed(MouseEvent e) 
                                    {x2=e.getX();
                                      y2=e.getY();
                                      draw=true;
                                    }
                                    public void mouseReleased(MouseEvent e)
                                   {draw=false;}
                                  }
                                );
  addMouseMotionListener(this);
  draw=false;
 }

//m�todos de la interface MouseMotionListener
 public void mouseDragged(MouseEvent e)
 {x1=e.getX();
  y1=e.getY();
  repaint();
 }

 public void mouseMoved(MouseEvent e)
 {showStatus("Moviendo mouse en: "+e.getX()+","+e.getY());}

public void paint(Graphics g)
 {if (draw) {g.drawLine(x1,y1,x2,y2);}
 }


}

