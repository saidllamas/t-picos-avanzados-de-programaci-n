import java.awt.*;
import java.awt.event.*;  // importa el paquete para el manejo de eventos

public class Pintar extends Frame 
{ int contador=0;
   Point [] p=new Point[1000]; // se establece la cantidad maxima de puntos a dibujar

   public Pintar() // m�todo constructor, si se tratar� de un applet, este seria el m�todo init()
  {super ("Pizarra de dibujo electronica"); // llama a la super clase (Frame) y pone titulo al frame
    add(new Label("arrastrar el raton para dibujar"),BorderLayout.SOUTH);  // incorpora la etiqueta al Frame
    addMouseMotionListener( new MouseMotionAdapter()  // se programa la captura del evento del rat�n
                                               { public void mouseDragged(MouseEvent e)
//cada vez que se arrastra el raton se va obteniendo la coordenada para despues dibujar ese punto                                              
                                                 { if (contador<p.length)
                                                    { p[contador]=e.getPoint(); 
                                                       contador++;
                                                       repaint();
                                                     }
                                                 } 
                                                }
                                              );
    setSize(600,600);   // se establece el tama�o del Frame
    setVisible(true);  // se establece que el Frame sea visible, si se env�a false como argumento, no se visualiza el Frame
  }
  public void paint(Graphics g)
  { super.paint(g);   // se llama al metodo paint de la superclase
     for(int i=0;i<p.length && p[i]!=null;i++)
     {g.fillOval(p[i].x,p[i].y,4,4);}
  } 
  public static void main (String arg[])
  {Pintar pizarra=new Pintar();
    pizarra.addWindowListener(new WindowAdapter() {public void windowClosing(WindowEvent e){System.exit(0);}});
  }
}
