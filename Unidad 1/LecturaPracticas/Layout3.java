// ejemplo GridLayout
import java.awt.*;
import java.awt.event.*;
public class Layout3 extends Panel
{public Layout3()
 {setLayout(new GridLayout(3,3));
  add(new Button("uno"));
  add(new Button("dos"));
  add(new Button("tres"));
  add(new Button("cuatro"));
  add(new Button("cinco"));
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout3());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}
