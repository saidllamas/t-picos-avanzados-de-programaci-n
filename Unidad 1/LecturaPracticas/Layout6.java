//Ejemplo GridBagLayout
import java.awt.*;
import java.awt.event.*;
//segundo ejemplo del uso del GridBaglayout

public class Layout6 extends Panel
{public Layout6()
 {GridBagLayout gb=new GridBagLayout();
  GridBagConstraints construye = new GridBagConstraints();
  setLayout(gb);
  // con weightx y weighty permite asignar un espaciado adicional a cada componente
  construye.weightx=2.0;
  construye.weighty=2.0;
  // fill permite expandir el componente para llenar el espacio que le ha sido asignado 
  // a traves de los valores que se establecieron con weightx y weighty
  // son cuatro constantes los valores que se le pueen asignar a fill:
  // GridBagConstraints.VERTICAL
  // GridBagConstraints.HORIZONTAL
  // GridBagConstraints.BOTH
  // GridBagConstraints.NONE
  construye.fill=GridBagConstraints.NONE;
  construye.gridx=0;
  construye.gridy=0;
  add(new Button("uno"),construye);
  construye.gridx=1;
  construye.gridy=0;
  add(new Button("dos"),construye);
  construye.gridx=1;
  construye.gridy=1;
  add(new Button("tres"),construye);
  construye.gridx=1;
  construye.gridy=2;
  add(new Button("cuatro"),construye);
  construye.gridx=2;
  construye.gridy=2;
  add(new Button("cinco"),construye);
  
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout6());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

