//Ejemplo CArdLayout
import java.awt.*;
import java.awt.event.*;

public class Layout12 extends Panel
{CardLayout cl=new CardLayout();
 Panel tarjetas=new Panel();
 Panel botones=new Panel();
 public Layout12()
 {Button b1,b2,b3,b4;
  setLayout(new BorderLayout());
  botones.add(b1=new Button("Primera"));
  botones.add(b2=new Button("Anterior"));
  botones.add(b3=new Button("Siguiente"));
  botones.add(b4=new Button("Ultima"));
  b1.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.first(tarjetas);}});
  b2.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.previous(tarjetas);}});
  b3.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.next(tarjetas);}});
  b4.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.last(tarjetas);}});
  add(botones,"South");
  tarjetas.setLayout(cl);
  tarjetas.add(new Button("Card 1"),"Tarjeta 1");
  tarjetas.add(new Button("Card 2"),"Tarjeta 2");
  tarjetas.add(new Button("Card 3"),"Tarjeta 3");  
  tarjetas.add(new Button("Card 4"),"Tarjeta 4");  
  tarjetas.add(new Button("Card 5"),"Tarjeta 5");  
  add(tarjetas,"Center");
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout12());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}