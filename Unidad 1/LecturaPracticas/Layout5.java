//Ejemplo GridBaglayout
import java.awt.*;
import java.awt.event.*;

//Ejemplo basico del GribagLayout

public class Layout5 extends Panel
{public Layout5()
 {//se crea un objeto de la clase GridBagLayout
  GridBagLayout gb=new GridBagLayout();
  //se crea un objeto de la clase GridBagConstraints
  //que es a traves de esta clase como vamos a ir construyendo la interface
  GridBagConstraints construye=new GridBagConstraints();
  // se establece que el administrador va a ser el GridBagLayout, se envia como 
  //parametro el objeto que creamos de la msisma
  setLayout(gb);
  // se indica la posicion donde se colocara el componente, gridx el indice 
  //de las x y gridy el indice de las y
/*   |     |      |
 0,0 | 1,0 |  2,0 | 3,0 
----------------------
 0,1 | 1,1 |  2,1 | 3,1
----------------------
 0,2 | 1,2 |  2,2 | 3,2
     |     |      |
*/
  construye.gridx=0;
  construye.gridy=0;
  // se agrega el componente al Grid
  // add(componente, objetoGridBagConstraints
  add(new Button("uno"),construye);
  construye.gridx=1;
  construye.gridy=0;
  // otra forma de agregar un componente al Grid, es siguiendo dos pasos
  gb.setConstraints(new Button("dos"),construye); //primer paso
  add(new Button("dos"));  //segundo paso
  // otra forma de agregar un componente al grid, es generando un metodo para ello
  addGB(new Button("tres"),1,1);
  // se terminan de agrear otros dos elementos
  construye.gridx=1;
  construye.gridy=2;
  add(new Button("cuatro"),construye);
  construye.gridx=2;
  construye.gridy=2;
  add(new Button("cinco"),construye);
  
 }
 //metodo generado para incrustar un componente al Grid
 public void addGB(Component componente, int x, int y)
 {GridBagConstraints gbc=new GridBagConstraints();
  gbc.gridx=x;
  gbc.gridy=x;
  add(componente,gbc);
 }
 
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout5());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

