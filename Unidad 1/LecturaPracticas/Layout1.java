//ejemplo FlowLayout
import java.awt.*;
import java.awt.event.*;
public class Layout1 extends Panel
{public Layout1()
 {setLayout(new FlowLayout(FlowLayout.LEFT,15,15));
  add(new Button("uno"));
  add(new Button("dos"));
  add(new Button("tres"));
  add(new Button("cuatro"));
  add(new Button("cinco"));
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout1());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}