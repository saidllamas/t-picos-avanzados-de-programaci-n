//ejemplo BoxLayout
import java.awt.*;
import java.applet.*;
import javax.swing.*;
public class pruebaBoxLayout extends Applet
{public void init()
 {// diferentes formatos del administrador BoxLayout
  // setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
  //setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
  //setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
  setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
 // se agregan los components al contenedor, la organziacion depende del administrador
// para este ejemplo solo se agregan botones, pero no hacen nada dado que no se esta programando la captura del evento y la generacion de las acciones
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
 }
}

