//ejemplo BorderLayout
import java.awt.*;
import java.awt.event.*;
public class Layout2 extends Panel
{public Layout2()
 {setLayout(new BorderLayout(15,15));
  add(new Button("uno"),"North");
  add(new Button("dos"),"West");
  add(new Button("tres"),"Center");
  add(new Button("cuatro"),"East");
  add(new Button("cinco"),"South");
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout2());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}