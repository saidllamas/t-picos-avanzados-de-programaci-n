//Ejemplo CardLayout
import java.awt.*;
import java.awt.event.*;

public class Layout8 extends Panel
{CardLayout cl=new CardLayout();
 Panel tarjetas=new Panel();
 Panel botones=new Panel();
 public Layout8()
 {Button b1,b2,b3,b4;
  setLayout(new BorderLayout());
  botones.add(b1=new Button("tarjeta 1"));
  botones.add(b2=new Button("tarjeta 2"));
  botones.add(b3=new Button("tarjeta 3"));

  b1.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.show(tarjetas,"Tarjeta 1");}});
  b2.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.show(tarjetas,"Tarjeta 2");}});
  b3.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){cl.show(tarjetas,"Tarjeta 3");}});
  add(botones,"South");
  tarjetas.setLayout(cl);
  tarjetas.add(new Button("Card 1"),"Tarjeta 1");
  tarjetas.add(new Button("Card 2"),"Tarjeta 2");
  tarjetas.add(new Button("Card 3"),"Tarjeta 3");  
  add(tarjetas,"Center");
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout8());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

