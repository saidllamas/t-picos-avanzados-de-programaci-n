//Ejemplo GridBagLayout
import java.awt.*;
import java.awt.event.*;

// Ejemplo 4 del uso del GridBagLayout

public class Layout7 extends Panel
{public Layout7()
 {GridBagLayout gb=new GridBagLayout();
  GridBagConstraints construye=new GridBagConstraints();
  setLayout(gb);
  construye.weightx=1.0;
  construye.weighty=1.0;
  construye.fill=GridBagConstraints.BOTH;
  // gridheight controla el numero de renglones a utilizar para la colocacion
  // de un componente
  construye.gridheight=2;
  construye.gridx=0;
  construye.gridy=0;
  add(new Button("uno"),construye);
  construye.gridheight=1;
  construye.gridx=1;
  construye.gridy=0;
  add(new Button("dos"),construye);
  construye.gridx=1;
  construye.gridy=1;
  add(new Button("tres"),construye);
  // gridwidth controla el numero de columnas a utilizar para la colocacion
  // de un componente
  construye.gridwidth=2;
  construye.gridx=0;
  construye.gridy=2;
  add(new Button("cuatro"),construye);
  construye.gridwidth=1;
  construye.gridheight=2;
  construye.gridwidth=2;
  construye.gridx=0;
  construye.gridy=3;
  add(new Button("cinco"),construye);
  
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout7());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

