//Box
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Layout4 extends Panel
{public Layout4()
 {//Container box=Box.createVerticalBox();
  Container box=Box.createHorizontalBox();
  setLayout(new BorderLayout());
  box.add(new Button("uno"));
  box.add(new Button("dos"));
  box.add(new Button("tres"));
  box.add(new Button("cuatro"));
  box.add(new Button("cinco"));
  add(box,"East");
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout4());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

