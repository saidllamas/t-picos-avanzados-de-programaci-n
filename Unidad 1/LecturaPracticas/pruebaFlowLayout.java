//ejemplo flowLayout
import java.awt.*;
import java.applet.*;
public class pruebaFlowLayout extends Applet
{public void init()
 {// diferentes formatos del administrador FlowLayout
  //setLayout(new FlowLayout(FlowLayout.LEFT));
  //setLayout(new FlowLayout(FlowLayout.RIGHT));
  //setLayout(new FlowLayout(FlowLayout.CENTER));
  setLayout(new FlowLayout(FlowLayout.CENTER,20,20));
 // se agregan los components al contenedor, la organziacion depende del administrador
// para este ejemplo solo se agregan botones, pero no hacen nada dado que no se esta programando la captura del evento y la generacion de las acciones
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
  add(new Button("Boton 1"));
 }
}
