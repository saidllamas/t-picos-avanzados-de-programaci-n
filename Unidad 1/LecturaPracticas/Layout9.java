//Ejemplo GridBagLayout
import java.awt.*;
import java.awt.event.*;
//tercer ejemplo del uso del GridBaglayout

public class Layout9 extends Panel
{public Layout9()
 {GridBagLayout gb=new GridBagLayout();
  GridBagConstraints construye=new GridBagConstraints();
  setLayout(gb);
  // con weightx y weighty permite asignar un espaciado adicional a cada componente
  construye.fill=GridBagConstraints.BOTH;
  construye.gridx=0;
  construye.gridy=0;
  construye.weightx=0.25;
  construye.weighty=0.25;
  add(new Button("uno"),construye);
  construye.gridx=1;
  construye.gridy=0;
  construye.weightx=0.5;
  construye.weighty=0.5;
  add(new Button("dos"),construye);
  construye.gridx=1;
  construye.gridy=1;
  construye.weightx=0.75;
  construye.weighty=0.75;
  add(new Button("tres"),construye);
  construye.gridx=1;
  construye.gridy=2;
  construye.weightx=1.0;
  construye.weighty=1.0;
  add(new Button("cuatro"),construye);
  construye.gridx=2;
  construye.gridy=2;
  construye.weightx=2.0;
  construye.weighty=2.0;
  add(new Button("cinco"),construye);
  
 }
 public static void main(String[] arg)
 {Frame f=new Frame();
  f.setBounds(20,20,400,300);
  f.add(new Layout9());
  f.setVisible(true);
  f.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
 }
}

