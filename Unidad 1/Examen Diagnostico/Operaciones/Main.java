/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 25/01/2017 como ejercicios de diagnostico
*	para la materia de topicos avanzados de programacion.
*	El siguiente procesa 4 diferentes operaciones, se auxulia de una clase que integra las 
* 	operaciones basicas: sumar-restar-multiplicar-dividir.
*	En esta clase solo se usa el main para ejecucion del programa, se crea una instancia
*	de la clase que apoyara para realizar las operaciones, utiliza 'double' y todos sus metodos
*	son publicos.
*/

class Main{
	public static void main(String[] args) {
		Operacion op = new Operacion();
		double res = op.sumar(2, op.multiplicar(3, op.multiplicar(4, op.dividir(5,2))));
		System.out.println("Operacion a) 2 + 3 * 4 * 5 / 2 = "+res);
		res = op.dividir(2, op.dividir(3, op.dividir(3, op.dividir(2, 1))));
		System.out.println("Operacion b) 2 / 3 / 3 / 2 / 1 = "+res);
		res = op.restar(2, op.sumar(4, op.restar(3, op.sumar(2, op.restar(5, 2)))));
		System.out.println("Operacion c) 2 - 4 + 3 - 2 + 5 - 2 = "+res);
		res = op.multiplicar(op.multiplicar(10, op.sumar(3, 5)), 13);
		System.out.println("Operacion d) 10 * (3 + 5) * 13 = "+res);
	}//end main
}//end class