/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 25/01/2017 como ejercicios de diagnostico
*	para la materia de topicos avanzados de programacion.
*	La siguiente unicamente recibe dos numeros decimales y devuelve la operacion deseada
*/

class Operacion{

	public double sumar(double num1, double num2){
		return num1 + num2;
	}//end sumar

	public double restar(double num1, double num2){
		return num1 - num2;
	}//end restar

	public double multiplicar(double num1, double num2){
		return num1 * num2;
	}//end multiplicar

	public double dividir(double num1, double num2){
		return num1 / num2;
	}//end dividir

}//end class