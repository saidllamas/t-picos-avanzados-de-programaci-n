import java.util.*;

/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 25/01/2017 como ejercicios de diagnostico
*	para la materia de topicos avanzados de programacion.
*	El siguiente lee una entrada por teclado y devuelve el numero de letras, digitos y caracteres que
*	la cadena contenga.
*	Se utilizan dos metodos, el main para iniciar el programa y uno mas para determinar la cantidad de
*	digitos, letras y caracteres.
*/

class Main{
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Ingrese una cadena: ");
		String cadena = entrada.nextLine();
		int[] longitud =  getCadena(cadena);
		System.out.println("Tiene "+longitud[0]+" letras ");
		System.out.println("Con "+longitud[1]+" digitos ");
		System.out.println("Y "+longitud[2]+" caracteres especiales.");
	}//end main

	private static int[] getCadena(String cadena){
		int[] longitud = new int[3];
		int nums = 0, chars = 0, lts = 0; 
		for(int i = 0; i < cadena.length(); i++){ //este for me ayuda a recorrer caracter x caracter de toda la cadena
			int ascii = (int)cadena.charAt(i);
			if(ascii >= 65 && ascii <= 90 || ascii >= 97 && ascii <= 122) //Mayusculas y minusculas
				lts++;
			else if(ascii >= 48 && ascii <= 57) //numeros
				nums++;
			else chars++; //todo lo demas
		}
		longitud[0] = lts; //letras
		longitud[1] = nums; //digitos
		longitud[2] = chars; //caracteres especiales
		return longitud;
	}//end getCadena
}//end class