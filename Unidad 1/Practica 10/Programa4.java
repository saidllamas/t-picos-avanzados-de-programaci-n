import java.awt.*;
import java.awt.event.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un frame o un applet en el cual se presente la letra
*  de una canci�n alusiva al d�a de las madres.
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa4 extends Frame implements ActionListener{

   TextArea tfLetra; 
   String letra; 

   public Programa4(){
      setLayout(null);//desactivo el gestor de layout

      letra = "\n\n";
      letra += "                                        Escucha mi cancion de amor \n                                        Mis cuitas y dolor\n";
      letra += "                                        Bien sabes tu que es para ti \n                                        La hice con la devocion\n";
      letra += "                                        Que es para mi una oracion\n\n\n ";
      letra += "                                       Madrecita tu nombre es Dios\n                                        Abnegacion luz y calor\n";
      letra += "                                        Felicidad adoracion\n                                        La reina del hogar\n\n\n";
      letra += "                                       Coro\n                                       Siempre tejiste con sonrisas\n";
      letra += "                                       Los amargos ratos y horas de dolor\n                                       Y ocultaste con valor\n";
      letra += "                                        Aquellas lagrimas de amor\n\n\n ";
      letra += "                                       Diste la sangre para que en mis venas\n                                        Yo tuviera siempre tu calor\n";
      letra += "                                      Tu nombre evoco madrecita ideal\n                                        Tu nombre evoco madrecita ideal\n\n\n";
      letra += "                                       Tu vida es mi inspiracion\n                                        Asi vivi con ilusion\n";
      letra += "                                       Viejita de mi ensonacion\n                                        Eres mi fe eres mi sol\n\n\n";
      letra += "                                       Quiero tenerte junto a mi\n                                        Para vivir feliz los dos\n";
      letra += "                                       Para cantar y en ti confiar\n                                       Mis cuitas y dolor\n\n\n\n";

      //construyendo elementos
      tfLetra = new TextArea();
      
      //ajustando elementos
      tfLetra.setBounds(10,35, 520,570);
      tfLetra.append(letra);
      tfLetra.setEditable(false);
      
      //escuchadores      
      
      //añadiendo al contenedor
      add(tfLetra);
   }//constuctor
   
   
   public void actionPerformed(ActionEvent e){
      /*if(e.getSource() == fr1){ 
      }*/
   }//actionPerformed
   
   public static void main(String[] args){
      Programa4 app = new Programa4();
      app.setBounds(460, 40, 550, 600); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tamaño
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            app.setVisible(false);
         }
      });
   }//main
}//class
