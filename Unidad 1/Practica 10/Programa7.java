import java.applet.*;
import java.awt.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un applet que dibuje un oso, un gato y un gusano
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa7 extends Applet{

   public static final Color YELLOW = new Color(255, 235, 59);
   public static final Color BLACK = new Color(33, 33, 33);
   public static final Color GREEN = new Color(76, 175, 80);
   public static final Color GREY = new Color(117, 117, 117);
   public static final Color BROWN = new Color(121, 85, 72);
   public static final Color BROWNl = new Color(121, 90, 72);
   public static final Color BROWNll = new Color(121, 104, 81);
   
   public void init(){
      resize(900, 300);//ejecuto a pantalla completa
      setBackground(new Color(255, 249, 196)); 
   }//end init
   
   public void paint(Graphics g){
      //Ajustes de pantalla
      int inicioX = -40, inicioY = 50;
      //Gusano
         //Cabeza
         g.setColor(GREEN);
         g.fillOval(inicioX+100,inicioY-20, 40,40);      
         g.setColor(BLACK);
         g.fillOval(inicioX+106, inicioY-10, 8, 8);
         g.drawArc(inicioX+103, inicioY, 14,8, 0,-180);//boca
         g.drawArc(inicioX+111,inicioY-33, 7,15, -270, 180);//antena
         //Cuerpo
         g.setColor(GREEN);
         g.fillOval(inicioX+128,inicioY+5, 40,40);
         g.fillOval(inicioX+166,inicioY+10, 40,40);
         g.fillOval(inicioX+204,inicioY+5, 40,40);
         g.fillOval(inicioX+242,inicioY+8, 40,40);
         g.fillOval(inicioX+280,inicioY+4, 40,40);
         g.setColor(BLACK);
         g.drawArc(inicioX+140,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+160,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+170,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+190,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+210,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+230,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+250,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+270,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+290,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+310,inicioY+36, 7,15, -270, 180);//pata
      //Oso
         g.setColor(BROWN);
         g.fillOval(inicioX+430, inicioY+40, 115,125); //panza
         g.setColor(BROWN);
         g.fillOval(inicioX+435, inicioY-19, 30,30); //oreja izq
         g.fillOval(inicioX+500, inicioY-19, 30,30); //oreja der
         g.setColor(BROWNll);
         g.fillOval(inicioX+445, inicioY-15, 80,80); //cabeza
         g.setColor(BROWNl);
         g.fillOval(inicioX+500, inicioY+100, 70,70); //pata der
         g.fillOval(inicioX+400, inicioY+100, 70,70); //pata izq
         g.setColor(Color.WHITE);
         g.fillOval(inicioX+445, inicioY-12, 10,10); //orifioreja izq
         g.fillOval(inicioX+513, inicioY-12, 10,10); //orifioreja der
         g.fillOval(inicioX+465, inicioY+10, 13,13); //ojo izq
         g.fillOval(inicioX+490, inicioY+10, 13,13); //ojo izq
         g.setColor(BLACK);
         g.fillOval(inicioX+425, inicioY+115, 17,17); //orifpata izq
         g.fillOval(inicioX+528, inicioY+122, 17,17); //orifpata der
      //Gato
         g.setColor(GREY);
         g.fillOval(inicioX+696, inicioY+35, 120, 140); //panza
         g.fillOval(inicioX+700, inicioY-15, 110, 80); //cabeza
         int puntoX[] = {inicioX+730, inicioX+704, inicioX+704};
         int puntoY[] = {inicioY, inicioY+8, inicioY-10};
         g.fillPolygon(puntoX, puntoY, 3); //Oreja izq
         int puntoXX[] = {inicioX+784, inicioX+804, inicioX+804};
         int puntoYY[] = {inicioY, inicioY+8, inicioY-10};
         g.fillPolygon(puntoXX, puntoYY, 3);
         g.setColor(Color.WHITE);
         g.fillOval(inicioX+725, inicioY+3, 9, 15); //ojo izq
         g.fillOval(inicioX+770, inicioY+3, 9, 15); //ojo der
         g.drawArc(inicioX+730, inicioY+27, 20,16, 0, -180); //boca
         g.drawArc(inicioX+750, inicioY+27, 20,16, 0, -180); //boca
         g.drawLine(inicioX+750, inicioY+35, inicioX+750, inicioY+29);
   }//end paint
   
}//class
