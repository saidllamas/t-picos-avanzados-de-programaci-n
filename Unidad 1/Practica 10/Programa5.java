import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Hacer un frame que permita realizar una encuesta a un usuario 
*  sobre la evaluaci�n de un producto, las preguntas son cerradas y solo tendr� 
*  que seleccionar de acuerdo a la pregunta bien sea una o varias casillas de 
*  verificaci�n, o un bot�n de radio, m�nimo 5 preguntas, una vez terminada la 
*  encuesta, dar a conocer cuales fueron las respuestas a manera de confirmaci�n 
*  y agradecer al usuario su participaci�n, el frame tambi�n contendr� dos botones, 
*  el primer bot�n ser� �nueva encuesta�, al presionarlo limpiar� los campos seleccionados 
*  por el usuario, para que el nuevo usuario seleccione sus valores (se repite el proceso 
*  descrito al incio), el segundo bot�n es �salir�, al presionarlo salir de la aplicaci�n 
*  de manera que no afecte al frame principal de donde ser� llamado �ste.
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa5 extends Frame implements ActionListener{
   
   Button btnNew, btnSalir;
   Checkbox cb1,cb2,cb3,cb4,cb5,cb6,cb7,cb8,cb9,cb10; 
   Checkbox cb11,cb12,cb13,cb14,cb15,cb16,cb17,cb18,cb19,cb20;
   Checkbox cb21,cb22,cb23,cb24,cb25;
   CheckboxGroup pr1 = new CheckboxGroup();
   CheckboxGroup pr2 = new CheckboxGroup();
   CheckboxGroup pr3 = new CheckboxGroup();
   CheckboxGroup pr4 = new CheckboxGroup();
   CheckboxGroup pr5 = new CheckboxGroup();

   
   public Programa5(){
      setLayout(null);
      setResizable(false);
      btnNew = new Button("Nueva encuesta");
      btnSalir = new Button("Salir");

      //bloque 1
      cb1 = new Checkbox("Mal", false, pr1);
      cb1.setBounds(20,58,40,25);
      cb2 = new Checkbox("Regular", false, pr1);
      cb2.setBounds(60,58,60,25);
      cb3 = new Checkbox("Normal", false, pr1);
      cb3.setBounds(125,58,60,25);
      cb4 = new Checkbox("Bien", false, pr1);
      cb4.setBounds(185,58,45,25);
      cb5 = new Checkbox("Muy bien", false, pr1);
      cb5.setBounds(230,58,80,25);
      //bloque 2
      cb6 = new Checkbox("Mal", false, pr2);
      cb6.setBounds(20,108,40,25);
      cb7 = new Checkbox("Regular", false, pr2);
      cb7.setBounds(60,108,60,25);
      cb8 = new Checkbox("Normal", false, pr2);
      cb8.setBounds(125,108,60,25);
      cb9 = new Checkbox("Bien", false, pr2);
      cb9.setBounds(185,108,45,25);
      cb10 = new Checkbox("Muy bien", false, pr2);
      cb10.setBounds(230,108,80,25);
      //bloque 3
      cb11 = new Checkbox("Mal", false, pr3);
      cb11.setBounds(20,158,40,25);
      cb12 = new Checkbox("Regular", false, pr3);
      cb12.setBounds(60,158,60,25);
      cb13 = new Checkbox("Normal", false, pr3);
      cb13.setBounds(125,158,60,25);
      cb14 = new Checkbox("Bien", false, pr3);
      cb14.setBounds(185,158,45,25);
      cb15 = new Checkbox("Muy bien", false, pr3);
      cb15.setBounds(230,158,80,25);
      //bloque 4
      cb16 = new Checkbox("Mal", false, pr4);
      cb16.setBounds(20,208,40,25);
      cb17 = new Checkbox("Regular", false, pr4);
      cb17.setBounds(60,208,60,25);
      cb18 = new Checkbox("Normal", false, pr4);
      cb18.setBounds(125,208,60,25);
      cb19 = new Checkbox("Bien", false, pr4);
      cb19.setBounds(185,208,45,25);
      cb20 = new Checkbox("Muy bien", false, pr4);
      cb20.setBounds(230,208,80,25);
      //bloque 5
      cb21 = new Checkbox("Mal", false, pr5);
      cb21.setBounds(20,258,40,25);
      cb22 = new Checkbox("Regular", false, pr5);
      cb22.setBounds(60,258,60,25);
      cb23 = new Checkbox("Normal", false, pr5);
      cb23.setBounds(125,258,60,25);
      cb24 = new Checkbox("Bien", false, pr5);
      cb24.setBounds(185,258,45,25);
      cb25 = new Checkbox("Muy bien", false, pr5);
      cb25.setBounds(230,258,80,25);

      btnSalir.setBounds(170, 320, 100, 23);
      btnNew.setBounds(40, 320, 100, 23);

      btnSalir.addActionListener(this);
      btnNew.addActionListener(this);

      add(btnNew);
      add(btnSalir);
      add(cb1);add(cb2);add(cb3);add(cb4);add(cb5);
      add(cb6);add(cb7);add(cb8);add(cb9);add(cb10);
      add(cb11);add(cb12);add(cb13);add(cb14);add(cb15);
      add(cb16);add(cb17);add(cb18);add(cb19);add(cb20);
      add(cb21);add(cb22);add(cb23);add(cb24);add(cb25);
   }//construct
   
   public void paint(Graphics g){
      //titulo, encuesta a estudiantes
      g.drawString("El profesor explica con claridad", 20,50);
      g.drawString("El nivel de exigencia academica es adecuado", 20, 100);
      g.drawString("Evaluacion justa de examenes y trabajos", 20, 150);
      g.drawString("El profesor respeta tus derechos", 20, 200);
      g.drawString("Inicio y finalizacion puntal de clase", 20, 250);
      g.setColor(Color.GRAY);
      g.drawLine(10, 300, 295, 300);
   }//paint
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == btnNew){
         //limpiar campos
         try{
            String msj = "El profesor explica con claridad: "+pr1.getSelectedCheckbox().getLabel();
            msj += "\nEl nivel de exigencia academica es adecuado: "+pr2.getSelectedCheckbox().getLabel();
            msj += "\nEvaluacion justa de examenes y trabajos: "+pr3.getSelectedCheckbox().getLabel();
            msj += "\nEl profesor respeta tus derechos: "+pr4.getSelectedCheckbox().getLabel();
            msj += "\nInicio y finalizacion puntal de clase: "+pr5.getSelectedCheckbox().getLabel();
            JOptionPane.showMessageDialog(null,"Gracias por participar. \n"+msj);
            //al final descubri el metodo setSelectedCheckbox(Checkbox box) pero lo dejare asi
            dispose();
            Programa5 app = new Programa5();
            app.setBounds(460, 40, 310, 390); //dimension del frame
            app.setResizable(false); //bloqueo ajuste de tamaño
            app.setVisible(true);
            app.addWindowListener(new WindowAdapter(){
               public void windowClosing(WindowEvent e){
                  System.exit(0);
               }
            });
         }catch(Exception d){
            JOptionPane.showMessageDialog(null,"Antes deberas responder todas las preguntas.");
         }
         
      }else if(e.getSource() == btnSalir){
         setVisible(false);
         //System.exit(0);
      }
   }//actionPerformed
   
   public static void main(String[] args){
      Programa5 app = new Programa5();
      app.setBounds(460, 40, 310, 390); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tamaño
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }//main
}//class