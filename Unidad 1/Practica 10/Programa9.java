import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un frame principal2, el cual integrar� las 7 preguntas 
*  anteriores, a trav�s de botones se tendr� acceso a cada una de esas preguntas, 
*  agregar dos botones m�s, uno para dar la opci�n para salir de la aplicaci�n, el *
*  segundo bot�n donde se presente la opci�n: acerca de, en la cual se presentaran 
*  sus datos: nombre, numero de control, semestre, materia
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa9 extends Frame implements ActionListener{
   
   Button btnProg1, btnProg2, btnProg3, btnProg4, btnProg5, btnProg6, btnProg7, btnSalir, btnInfo;

   public Programa9(){
      setLayout(null);
      MenuBar menuBar = new MenuBar();
      Menu mnClases = new Menu("Clases");
      Menu mnInformacion = new Menu("Infomacion");
            
      btnProg1 = new Button("Programa 1");
      btnProg2 = new Button("Programa 2");
      btnProg3 = new Button("Programa 3");
      btnProg4 = new Button("Programa 4");
      btnProg5 = new Button("Programa 5");
      btnProg6 = new Button("Programa 6");
      btnProg7 = new Button("Programa 7");
      btnSalir = new Button("Salir");
      btnInfo = new Button("Acerca de");


      btnProg1.setBounds(20,50,100,23);
      btnProg2.setBounds(20,90,100,23);
      btnProg3.setBounds(20,130,100,23);
      btnProg4.setBounds(20,170,100,23);
      btnProg5.setBounds(20,210,100,23);
      btnProg6.setBounds(20,250,100,23);
      btnProg7.setBounds(20,290,100,23);

      
      btnInfo.setBounds(20,330,100,23);
      btnSalir.setBounds(20,370,100,23);

    
      btnProg1.addActionListener(this);
      btnProg2.addActionListener(this);
      btnProg3.addActionListener(this);
      btnProg4.addActionListener(this);
      btnProg5.addActionListener(this);
      btnProg6.addActionListener(this);
      btnProg7.addActionListener(this);
      btnSalir.addActionListener(this);
      btnInfo.addActionListener(this);
      

      add(btnInfo);
      add(btnSalir);
      add(btnProg1);
      add(btnProg2);
      add(btnProg3);
      add(btnProg4);
      add(btnProg5);
      add(btnProg6);
      add(btnProg7);

   }//end construct
   
   public void actionPerformed(ActionEvent a){
      if(a.getSource() == btnSalir) System.exit(0);
      else if(a.getSource() == btnProg1){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa1.html");
         }catch (Exception e) {}
      }else if(a.getSource() == btnProg2){
         Programa2 app = new Programa2();
         app.setBounds(0,0,800,470);
         app.setVisible(true);     
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == btnProg3){
         Programa3 app = new Programa3();
         app.setBounds(460, 40, 400, 360); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == btnProg4){
         Programa4 app = new Programa4();
         app.setBounds(460, 40, 550, 600); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == btnProg5){
         Programa5 app = new Programa5();
         app.setBounds(460, 40, 310, 390); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == btnProg6){
         Frame app = new Frame();
         app.setBounds(10,10,300, 370);
         app.add(new Programa6());
         app.setResizable(false);
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == btnProg7){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa7.html");
         }catch (Exception e) {}
      }else if(a.getSource() == btnInfo){
         JOptionPane.showMessageDialog(this, "Informacion general \nJesus Said Llamas Manriquez\n15290902\n4 Semestre\nTopicos Avanzados de Programacion");
      }
   }//end actionPerformed
   
   public static void main(String[] args){
     Programa9 app = new Programa9();
     app.setBounds(100,30,140, 450);
     app.setResizable(false);
     app.setVisible(true);
     app.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
            System.exit(0);
      }
     });
   }//end main
}//end class