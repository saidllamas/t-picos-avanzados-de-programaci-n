import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un frame principal4, el cual integrar� las 7 preguntas
*  anteriores, a trav�s de una lista se tendr� acceso a cada una de esas preguntas, 
*  agregar elementos m�s, uno para dar la opci�n para salir de la aplicaci�n, y otro 
*  donde se presente la opci�n: acerca de, en la cual se presentaran sus datos: nombre, 
*  numero de control, semestre, materia
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa11 extends Frame implements ItemListener{
   
   Choice chOpciones;

   public Programa11(){
      setLayout(null);
      
      chOpciones = new Choice();

      chOpciones.add("");
      chOpciones.add("Programa 1");
      chOpciones.add("Programa 2");
      chOpciones.add("Programa 3");
      chOpciones.add("Programa 4");
      chOpciones.add("Programa 5");
      chOpciones.add("Programa 6");
      chOpciones.add("Programa 7");
      chOpciones.add("Salir");
      chOpciones.add("Acerca de");


      chOpciones.setBounds(20,50,100,23);

      chOpciones.addItemListener(this);

      add(chOpciones);

   }//end construct
   
   public void itemStateChanged(ItemEvent e){
      if(e.getItem() == "Salir") System.exit(0);
      else if(e.getItem() == "Programa 1"){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa1.html");
         }catch (Exception i) {}
      }else if(e.getItem() == "Programa 2"){
         Programa2 app = new Programa2();
         app.setBounds(0,0,800,470);
         app.setVisible(true);     
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(e.getItem() == "Programa 3"){
         Programa3 app = new Programa3();
         app.setBounds(460, 40, 400, 360); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(e.getItem() == "Programa 4"){
         Programa4 app = new Programa4();
         app.setBounds(460, 40, 550, 600); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(e.getItem() == "Programa 5"){
         Programa5 app = new Programa5();
         app.setBounds(460, 40, 310, 390); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(e.getItem() == "Programa 6"){
         Frame app = new Frame();
         app.setBounds(10,10,300, 370);
         app.add(new Programa6());
         app.setResizable(false);
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(e.getItem() == "Programa 7"){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa7.html");
         }catch (Exception i) {}
      }else if(e.getItem() == "Acerca de"){
         JOptionPane.showMessageDialog(this, "Informacion general \nJesus Said Llamas Manriquez\n15290902\n4 Semestre\nTopicos Avanzados de Programacion");
      }
   }//end actionPerformed
   
   public static void main(String[] args){
     Programa11 app = new Programa11();
     app.setBounds(600,30,140, 150);
     app.setResizable(false);
     app.setVisible(true);
     app.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
         System.exit(0);
      }
     });
   }//end main
}//end class