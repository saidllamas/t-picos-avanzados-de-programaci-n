import java.awt.*;
import java.awt.event.*;
import java.applet.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Imitar una interfaz proporcionada
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa6 extends Panel implements ActionListener{
   Button b, b2, b3, b4;
   Label lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9;
   TextField txtUser, txtName, txtID, txtHome, txtPass, txtPassConfirm;
   Choice group, login;
   Button btnOk, btnCancel;

   public Programa6(){
      setLayout(null);

      lbl1 = new Label("Create new user");
      lbl1.setBounds(10,20,110,15);
      lbl2 = new Label("User Name:");
      lbl2.setBounds(10,50,110,15);
      txtUser = new TextField();
      txtUser.setBounds(125, 50, 150, 23);
      lbl3 = new Label("Full Name:");
      lbl3.setBounds(10,80,110,15);
      txtName = new TextField();
      txtName.setBounds(125, 80, 150, 23);
      lbl4 = new Label("User ID:");
      lbl4.setBounds(10,110,110,15);
      txtID = new TextField("(automatic)");
      txtID.setBounds(125, 110, 150, 23);
      txtID.setEditable(false);
      lbl5 = new Label("Group:");
      lbl5.setBounds(10,140,110,15);
      group = new Choice();
      group.add("staff");
      group.add("admin");
      group.add("users");
      group.setBounds(125, 140, 110,10);
      lbl6 = new Label("Home Directory");
      lbl6.setBounds(10,170,110,15);
      txtHome = new TextField("(automatic)");
      txtHome.setBounds(125, 170, 150, 23);
      txtHome.setEditable(false);
      lbl7 = new Label("Login Shell");
      lbl7.setBounds(10,200,110,15);
      login = new Choice();
      login.add("/bin/ssh");
      login.add("/secure/asp");
      login.add("/cpanel/bin");
      login.add("/cgi");
      login.setBounds(125, 200, 110,10);
      lbl8 = new Label("Password:");
      lbl8.setBounds(10,230,110,15);
      txtPass = new TextField("(automatic)");
      txtPass.setBounds(125, 230, 150, 23);
      txtPass.setEchoChar('*');
      lbl9 = new Label("Confirm:");
      lbl9.setBounds(10,260,110,15);
      txtPassConfirm = new TextField("(automatic)");
      txtPassConfirm.setBounds(125, 260, 150, 23);
      txtPassConfirm.setEchoChar('*');

      btnOk = new Button("Ok");
      btnOk.setBounds(237, 300, 40, 23);
      btnCancel = new Button("Cancel");
      btnCancel.setBounds(158, 300, 70,23);
      
      add(lbl1);add(lbl2);add(lbl3);add(lbl4);add(lbl5);add(lbl6);add(lbl7);add(lbl8);add(lbl9);
      add(txtUser);add(txtName);add(txtID);add(group);add(txtHome);add(login);add(txtPass);add(txtPassConfirm);
      add(btnOk);add(btnCancel);
   }
   
   public static void main(String[] args){
      Frame app = new Frame();
		app.setBounds(10,10,300, 370);
	   app.add(new Programa6());
      app.setResizable(false);
		app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            app.setVisible(false);
         }
      });
   }
   
   public void actionPerformed(ActionEvent e){
      //if(e.getSource() == b)
   }//actionPerformed
}//class