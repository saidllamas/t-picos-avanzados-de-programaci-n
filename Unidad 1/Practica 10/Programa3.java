import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un frame que permita registrar los libros que se tienen
*  en la biblioteca (titulo, autor, editoira, No paginas), este frame contendr�
*  tres botones, el primero ser� �guardar datos�, al presionarlo se enviar� el 
*  mensaje (a trav�s de un JOptionPane (cuadro de dialogo)) �libro almacenado�,; 
*  el segundo bot�n ser� �mostrar datos�, al presionarlo se mostrar�n todos los datos 
*  capturados (libros), y el tercer bot�n dera el de salir, el cual cerrar� la aplicaci�n
*  (una vez que �ste correcta �ste frame, la opci�n de cerrar cambiarla para que solo oculte
*  el frame, ya que este frame formara parte de un frame principal).
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa3 extends Frame implements ActionListener{
   
   TextField txtTitulo, txtAutor, txtEditorial, txtNPgs;
   Button btnGuardar, btnMostrar, btnSalir;
   TextArea log;
   
   public Programa3(){
      setLayout(null);
      setResizable(false); //bloqueo ajuste de tamaño
      
      txtTitulo = new TextField();
      txtAutor = new TextField();
      txtEditorial = new TextField();
      txtNPgs = new TextField();
      btnGuardar = new Button("Guardar");
      btnMostrar = new Button("Mostrar datos");
      btnSalir = new Button("Salir");
      log = new TextArea();

      
      txtTitulo.setBounds(20, 60, 160, 22);
      txtAutor.setBounds(200, 60, 160, 22);
      txtEditorial.setBounds(20, 110, 160, 22);
      txtNPgs.setBounds(200, 110, 160, 22);
      btnGuardar.setBounds(30, 150, 100, 22);
      btnMostrar.setBounds(150, 150, 100, 22);
      btnSalir.setBounds(270, 150, 100, 22);
      log.setBounds(20, 190, 350, 130);
      log.setEditable(false);
      
      btnGuardar.addActionListener(this);
      btnMostrar.addActionListener(this);
      btnSalir.addActionListener(this);
      
      add(txtTitulo);
      add(txtAutor);
      add(txtEditorial);
      add(txtNPgs);
      add(btnGuardar);
      add(btnMostrar);
      add(btnSalir);
      add(log);
   }//construc
   
   public void paint(Graphics g){
      g.drawString("Titulo: ", 20,50);
      g.drawString("Autor: ", 203, 50);
      g.drawString("Editorial: ", 20, 105);
      g.drawString("No. Paginas: ", 203, 105);
   }//paint
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == btnGuardar){
         log.insert(txtTitulo.getText()+" "+txtAutor.getText()+" "+txtEditorial.getText()+" "+txtNPgs.getText()+"\n", 0);
         JOptionPane.showMessageDialog(null,"Libro almacenado.");
      }else if(e.getSource() == btnSalir){
         setVisible(false);
         //System.exit(0);
      }else if(e.getSource() == btnMostrar){
         JOptionPane.showMessageDialog(null, log.getText());
      }
   }//actionPerformed
   
   public static void main(String[] args){
      Programa3 app = new Programa3();
      app.setBounds(460, 40, 400, 360); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tama�o
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            app.setVisible(false);
         }
      });
   }//main
}//class