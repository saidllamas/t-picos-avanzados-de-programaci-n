import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un frame principal, el cual integrar� las 7 preguntas 
*  anteriores, a trav�s de una barra de menus, se tendr�n dos men�s; el primero 
*  presentar�s las diferentes opciones que permitan activar cualquiera de las 
*  aplicaciones anteriores, y agregar una opci�n m�s para salir de la aplicaci�n, 
*  el segundo men� donde se presente la opci�n: acerca de, en la cual se presentaran 
*  sus datos: nombre, numero de control, semestre, materia
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa8 extends Frame implements ActionListener{
   
   MenuItem miProg1, miProg2, miProg3, miProg4, miProg5, miProg6, miProg7, miSalir, miInfo;

   public Programa8(){
      MenuBar menuBar = new MenuBar();
      Menu mnClases = new Menu("Clases");
      Menu mnInformacion = new Menu("Infomacion");
            
      miProg1 = new MenuItem("Programa 1");
      miProg2 = new MenuItem("Programa 2");
      miProg3 = new MenuItem("Programa 3");
      miProg4 = new MenuItem("Programa 4");
      miProg5 = new MenuItem("Programa 5");
      miProg6 = new MenuItem("Programa 6");
      miProg7 = new MenuItem("Programa 7");
      miSalir = new MenuItem("Salir", new MenuShortcut(KeyEvent.VK_S));
      
      mnClases.add(miProg1);
      mnClases.add(miProg2);
      mnClases.add(miProg3);
      mnClases.add(miProg4);
      mnClases.add(miProg5);
      mnClases.add(miProg6);
      mnClases.add(miProg7);
      mnClases.add(miSalir);

      miInfo = new MenuItem("Acerca de");
      mnInformacion.add(miInfo);
    
      miProg1.addActionListener(this);
      miProg2.addActionListener(this);
      miProg3.addActionListener(this);
      miProg4.addActionListener(this);
      miProg5.addActionListener(this);
      miProg6.addActionListener(this);
      miProg7.addActionListener(this);
      miSalir.addActionListener(this);
      miInfo.addActionListener(this);
      

      menuBar.add(mnClases);
      menuBar.add(mnInformacion);
      
      setMenuBar(menuBar);
   }//end construct
   
   public void actionPerformed(ActionEvent a){
      if(a.getSource() == miSalir) System.exit(0);
      else if(a.getSource() == miProg1){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa1.html");
         }catch (Exception e) {}
      }else if(a.getSource() == miProg2){
         Programa2 app = new Programa2();
         app.setBounds(0,0,800,470);
         app.setVisible(true);     
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == miProg3){
         Programa3 app = new Programa3();
         app.setBounds(460, 40, 400, 360); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == miProg4){
         Programa4 app = new Programa4();
         app.setBounds(460, 40, 550, 600); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == miProg5){
         Programa5 app = new Programa5();
         app.setBounds(460, 40, 310, 390); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == miProg6){
         Frame app = new Frame();
         app.setBounds(10,10,300, 370);
         app.add(new Programa6());
         app.setResizable(false);
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(a.getSource() == miProg7){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa7.html");
         }catch (Exception e) {}
      }else if(a.getSource() == miInfo){
         JOptionPane.showMessageDialog(this, "Informacion general \nJesus Said Llamas Manriquez\n15290902\n4 Semestre\nTopicos Avanzados de Programacion");
      }
   }//end actionPerformed
   
   public static void main(String[] args){
     Programa8 app = new Programa8();
     app.setBounds(100,30,800, 500);
     app.setResizable(false);
     app.setVisible(true);
     app.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
            System.exit(0);
      }
     });
   }//end main
}//end class