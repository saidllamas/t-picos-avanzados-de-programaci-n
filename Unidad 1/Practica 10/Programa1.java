import java.applet.*;
import java.awt.*;
import java.awt.event.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Hacer una applet el cual contendr� dos botones de radio, 
*  la selecci�n de estos botones colocaran en cualquiera de los dos extremos
*  siguientes a una pelotita, izquierda o derecha, dependiendo de la opci�n 
*  seleccionada ser� la ubicaci�n de la pelotita.
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa1 extends Applet implements ItemListener{
	
	Checkbox rdIzquierda, rdDerecha;
	CheckboxGroup alineacion = new CheckboxGroup();
	int posX = 140;
   
   public Programa1(){
      setLayout(null);
      rdIzquierda = new Checkbox("Derecha", false, alineacion);
      rdIzquierda.setBounds(100,10,80,23);
      rdDerecha = new Checkbox("Izquierda", false, alineacion);
      rdDerecha.setBounds(10,10,80,23);

      rdDerecha.addItemListener(this);
      rdIzquierda.addItemListener(this);
         
      add(rdIzquierda);
      add(rdDerecha);
   }

   public void itemStateChanged(ItemEvent e){
   	if(rdDerecha.getState()){
   		posX = 10;
   	}else if(rdIzquierda.getState()){
   		posX = 270;
   	}
	repaint();
   }


	public void paint(Graphics g){
		g.fillOval(posX, 100, 80, 80);
	}
}