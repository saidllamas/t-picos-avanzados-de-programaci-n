import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Dise�ar un frame principal3, el cual integrar� las 7 preguntas
*  anteriores, a trav�s de botones de radio se tendr� acceso a cada una de esas 
*  preguntas, agregar dos botones de radio m�s, uno para dar la opci�n para salir 
*  de la aplicaci�n, el segundo donde se presente la opci�n: acerca de, en la cual 
*  se presentaran sus datos: nombre, numero de control, semestre, materia
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa10 extends Frame implements ItemListener{
   
   Checkbox chProg1, chProg2, chProg3, chProg4, chProg5, chProg6, chProg7, chSalir, chInfo;
   CheckboxGroup opc = new CheckboxGroup();

   public Programa10(){
      setLayout(null);
            
      chProg1 = new Checkbox("Programa 1", false, opc);
      chProg2 = new Checkbox("Programa 2", false, opc);
      chProg3 = new Checkbox("Programa 3", false, opc);
      chProg4 = new Checkbox("Programa 4", false, opc);
      chProg5 = new Checkbox("Programa 5", false, opc);
      chProg6 = new Checkbox("Programa 6", false, opc);
      chProg7 = new Checkbox("Programa 7", false, opc);
      chSalir = new Checkbox("Salir", false, opc);
      chInfo = new Checkbox("Acerca de", false, opc);


      chProg1.setBounds(20,50,100,23);
      chProg2.setBounds(20,90,100,23);
      chProg3.setBounds(20,130,100,23);
      chProg4.setBounds(20,170,100,23);
      chProg5.setBounds(20,210,100,23);
      chProg6.setBounds(20,250,100,23);
      chProg7.setBounds(20,290,100,23);

      
      chInfo.setBounds(20,330,100,23);
      chSalir.setBounds(20,370,100,23);

    
      chProg1.addItemListener(this);
      chProg2.addItemListener(this);
      chProg3.addItemListener(this);
      chProg4.addItemListener(this);
      chProg5.addItemListener(this);
      chProg6.addItemListener(this);
      chProg7.addItemListener(this);
      chSalir.addItemListener(this);
      chInfo.addItemListener(this);
      

      add(chInfo);
      add(chSalir);
      add(chProg1);
      add(chProg2);
      add(chProg3);
      add(chProg4);
      add(chProg5);
      add(chProg6);
      add(chProg7);

   }//end construct
   
   public void itemStateChanged(ItemEvent e){
      if(chSalir.getState()) System.exit(0);
      else if(chProg1.getState()){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa1.html");
         }catch (Exception i) {}
      }else if(chProg2.getState()){
         Programa2 app = new Programa2();
         app.setBounds(0,0,800,470);
         app.setVisible(true);     
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(chProg3.getState()){
         Programa3 app = new Programa3();
         app.setBounds(460, 40, 400, 360); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(chProg4.getState()){
         Programa4 app = new Programa4();
         app.setBounds(460, 40, 550, 600); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(chProg5.getState()){
         Programa5 app = new Programa5();
         app.setBounds(460, 40, 310, 390); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(chProg6.getState()){
         Frame app = new Frame();
         app.setBounds(10,10,300, 370);
         app.add(new Programa6());
         app.setResizable(false);
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               app.setVisible(false);
            }
         });
      }else if(chProg7.getState()){
         try{
            Runtime.getRuntime().exec(" appletviewer Programa7.html");
         }catch (Exception i) {}
      }else if(chInfo.getState()){
         JOptionPane.showMessageDialog(this, "Informacion general \nJesus Said Llamas Manriquez\n15290902\n4 Semestre\nTopicos Avanzados de Programacion");
      }
   }//end actionPerformed
   
   public static void main(String[] args){
     Programa10 app = new Programa10();
     app.setBounds(100,30,140, 450);
     app.setResizable(false);
     app.setVisible(true);
     app.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
         System.exit(0);
      }
     });
   }//end main
}//end class