import java.awt.*;
import java.awt.event.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Hacer un applet que contenga 4 barras de desplazamiento, 
*  las cuales representar�n las coordenadas x y y, y el tama�o de la figura
*  w(ancho) y h(alto), al desplazar estas barras, los nuevos valores aplicarlos
*  a una estrella de 6 picos rellena. 
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Programa2 extends Frame implements AdjustmentListener{
   
   Scrollbar sbX, sbY, valW, valH;
   int x, y, w, h;
   int inX = 580, inY = 200;
   
   public Programa2(){
      setLayout(null);

      sbX = new Scrollbar(Scrollbar.VERTICAL, 0, 1, -110, 110);
      sbY = new Scrollbar(Scrollbar.VERTICAL, 0, 1, -120, 145);
      valW = new Scrollbar(Scrollbar.VERTICAL, 0, 1,0, 20);
      valH = new Scrollbar(Scrollbar.VERTICAL, 0, 1,0, 20);
      
      sbX.setBounds(40,60,60,350);
      sbY.setBounds(120,60,60,350);
      valW.setBounds(200,60,60,350);
      valH.setBounds(280,60,60,350);
      
      add(sbX);
      add(sbY);
      add(valW);
      add(valH);
      
      sbX.addAdjustmentListener(this);
      sbY.addAdjustmentListener(this);
      valW.addAdjustmentListener(this);
      valH.addAdjustmentListener(this);
   }//end constuctor
   
   public void adjustmentValueChanged(AdjustmentEvent e){
      x = sbX.getValue();
      y = sbY.getValue();
      w = valW.getValue();
      h = valH.getValue();
      repaint();
   }// end adjustemValueChanged
   
   public void paint(Graphics g){
      g.drawRect(380, 60, 385, 385); //area de dibujo
      g.drawString("X "+x, 60, 430);
      g.drawString("Y "+y, 141, 430);
      g.drawString("W "+w, 221, 430);
      g.drawString("H "+h, 301, 430);
      int puntoX[] = {inX + x, inX+20 + x+w, inX+50 + x+w+w, inX+30 + x+w, inX+50 + x+w+w, inX+20 + x+w, inX + x, inX-20 + x-w, inX-50 + x-w-w, inX-30 + x-w, inX-50 + x-w-w, inX-20 + x-w};
      int puntoY[] = {inY+y-h, inY+20+y, inY+20+y-h, inY+40+y, inY+60+y+h,   inY+60+y,   inY+80+y+h, inY+60+y,  inY+60+y+h,   inY+40+y,   inY+20+y-h,   inY+20+y};
      g.fillPolygon(puntoX, puntoY, 12);
   }//end paint
   
   public static void main(String[] args){
      Programa2 app = new Programa2();
      app.setBounds(0,0,800,470);
      app.setVisible(true);     
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }//window closing
      });//window listener
   }//main
}//clas