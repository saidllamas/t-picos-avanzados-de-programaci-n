import java.awt.*;
import javax.swing.*;
import java.applet.*;
import java.awt.event.*;

public class EjemGUI13 extends Panel{

   public EjemGUI13(){
      setLayout(new BorderLayout());
      Panel norte = new Panel();
      norte.setLayout(new GridLayout(1,2));
      Panel n1 = new Panel();
      Panel n2 = new Panel();
      
      n1.setLayout(new FlowLayout(FlowLayout.LEFT));
      n1.add(new Button("btn"));
      n1.add(new Button("btn"));
      n1.add(new Button("btn"));
     //cambiar alineacion a derecha -izq
     n2.setLayout(new FlowLayout(FlowLayout.RIGHT));
     n2.add(new Button("btn"));
     n2.add(new Button("btn"));
     norte.add(n1);
     norte.add(n2);
     
     Panel sur = new Panel();
     sur.add(new Button("btn"));
     sur.add(new Button("btn"));
     sur.add(new Button("btn"));
     
     Panel este = new Panel();
     este.setLayout(new BoxLayout(este, BoxLayout.Y_AXIS));
     este.add(new Button("btn"));
     este.add(new Button("btn"));
     este.add(new Button("btn"));
     este.add(new Button("btn"));
     
     Panel oeste = new Panel();
     oeste.setLayout(new BoxLayout(oeste, BoxLayout.Y_AXIS));
     oeste.add(new Button("btn"));
     oeste.add(new Button("btn"));
     oeste.add(new Button("btn"));
     oeste.add(new Button("btn"));
     
     Panel centro = new Panel();
     centro.setLayout(new GridLayout(2,2));
     Panel p1 = new Panel();
     p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
     Panel p2 = new Panel();
     p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
     Panel p3 = new Panel();
     p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
     Panel p4 = new Panel();
     p4.setLayout(new BoxLayout(p4, BoxLayout.X_AXIS));
     
     p1.add(new Button("bn"));
     p1.add(new Button("bn"));
     p1.add(new Button("bn"));
     p2.add(new Button("bn"));
     p2.add(new Button("bn"));
     p2.add(new Button("bn"));
     p3.add(new Button("bn"));
     p3.add(new Button("bn"));
     p3.add(new Button("bn"));
     p4.add(new Button("bn"));
     p4.add(new Button("bn"));
     p4.add(new Button("bn"));

     centro.add(p1);
     centro.add(p2);
     centro.add(p3);
     centro.add(p4);
     
     add(norte, BorderLayout.NORTH); //norte
     add(sur, BorderLayout.SOUTH);
     add(este, BorderLayout.EAST);
     add(oeste, BorderLayout.WEST);
     add(centro, BorderLayout.CENTER);
   }//constructor
   
   public static void main(String[] args){
      Frame f = new Frame();
		f.setBounds(10,10,500,500);
	   f.add(new EjemGUI13());
		f.setVisible(true);
      f.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
      		System.exit(0);
      }});
   }//main
}//class
