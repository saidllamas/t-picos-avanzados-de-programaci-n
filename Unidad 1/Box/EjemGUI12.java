import java.awt.*;
import javax.swing.*;
import java.applet.*;

public class EjemGUI12 extends Applet{
   public void init(){
      setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
      Panel w = new Panel();
      w.setLayout(new BoxLayout(w, BoxLayout.X_AXIS));
      w.add(new Button("b1"));
      w.add(new Button("b1"));
      w.add(new Button("b1"));
      w.add(new Button("b1"));
      add(w);
      Panel p = new Panel();
      p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
      p.add(new Button("b2"));
      add(p);
      Panel q = new Panel();
      q.setLayout(new BoxLayout(q, BoxLayout.X_AXIS));
      q.add(new Button("b3"));
      q.add(new Button("b3"));
      q.add(new Button("b3"));
      q.add(new Button("b3"));
      add(q);
      Panel c = new Panel();
      c.setLayout(new BoxLayout(c, BoxLayout.Y_AXIS));
      c.add(new Button("b4"));
      add(c);
      Panel e = new Panel();
      e.setLayout(new BoxLayout(e, BoxLayout.X_AXIS));
      e.add(new Button("b5"));
      e.add(new Button("b5"));
      e.add(new Button("b5"));
      e.add(new Button("b5"));
      add(e);
   }//init
}//class