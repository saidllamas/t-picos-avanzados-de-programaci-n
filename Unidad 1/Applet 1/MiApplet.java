import java.awt.*;
import java.applet.*;

public class MiApplet extends Applet{
      public void paint(Graphics g){
      Color morado = new Color(100,100,0);
      
      g.drawString("Hola mundo", 50,50);
      
      g.setColor(Color.pink);
      g.drawString("J. Said Llamas Manriquez",50,75);
      
      g.drawString("15290902",50,100);
      g.setColor(morado); // ó g.setColor(new Color(100,100,0));
      
      g.drawString("4 Semestre",50,125);
      g.drawString("****",50,150);
      
      g.setColor(new Color((int)((float)Math.random()*255),(int)((float)Math.random()*255),0));
      g.drawString("Color aleatorio",50,175);
  
      }
      public void init() {
        setBackground(Color.blue);
        setForeground(Color.yellow);
      }
}