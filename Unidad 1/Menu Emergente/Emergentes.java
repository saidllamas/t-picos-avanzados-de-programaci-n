import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Emergentes extends Frame implements ActionListener{
   Frame f;
   PopupMenu m1, m2;
   MenuItem op1, op2, op3;
   
   public Emergentes(){
      setLayout(new FlowLayout());
      m1 = new PopupMenu("Menu 1");
      m2 = new PopupMenu("Menu 2");
      op1 = new MenuItem("Opcion 1");
      op2 = new MenuItem("Opcion 2");
      op3 = new MenuItem("Salir", new MenuShortcut(KeyEvent.VK_X));
      
      m1.add(op1);
      m1.addSeparator();
      m1.add(op3);
      m2.add(op2);
      add(m1);
      add(m2);  
      setFont(new Font("",0,24));
      add(new Label("Ejemplo De Un Menu Emergente"));
      op1.addActionListener(this);
      op2.addActionListener(this);
      op3.addActionListener(this);
      addMouseListener(new Posicion(this, m1));
      addMouseListener(new Posicion(this, m2));
      setSize(500,500);
      setVisible(true);   
   }//Emergente
   
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == op1){
         JOptionPane.showMessageDialog(this, "Soy La Opcion 1");
      }else if(e.getSource() == op2){
         f = new Frame();
         f.setVisible(true);
         f.setSize(150, 350);
         
         f.addWindowListener(new WindowAdapter(){     
            public void windowClosing(WindowEvent e){
               f.dispose();
               f.setVisible(false);
               f = null;
            }
         });
         //JOptionPane.showMessageDialog(this, "Soy La Opcion 2");
         }else if(e.getSource() == op3){
            JOptionPane.showMessageDialog(this, "Hasta Luego");
            System.exit(0);
         }
   }//actionPerformed
   
   
   public static void main(String[] args){
      new Emergentes();
   }//main
   
   
   class Posicion extends MouseAdapter{
      Frame f;
      PopupMenu p;
      
      public Posicion(Frame f, PopupMenu p){
         this.f = f;
         this.p = p;
      }//Posicion
      
      
      public void mousePressed(MouseEvent e){
//         if((p.getLabel().equals("Menu 1") && (e.getButton() == MouseEvent.BUTTON1)) || (p.getLabel().equals("Menu 2") && (e.getButton() == MouseEvent.BUTTON3))){
            p.show(f, e.getX(), e.getY());
//         }//if
      }//mousePressed
   }//Posicion
}//class