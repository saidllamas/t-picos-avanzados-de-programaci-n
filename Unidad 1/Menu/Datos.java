import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;

public class Datos extends Applet implements ActionListener{
   String nc, nomb, clave;
   TextField d1, d2, d3;
   TextArea ta;
   
   public void init(){
      d1 = new TextField(9);      
      d2 = new TextField("Nombre ",30); 
      d3 = new TextField("Clave"); 
      d3.setEchoChar('X'); 
      
      ta = new TextArea(10,40);
      
      d1.addActionListener(this);
      d2.addActionListener(this);
      d3.addActionListener(this);
      add(new Label("N. de control"));
      add(d1);
      add(new Label("Nombre"));
      add(d2);
      add(new Label("Clave"));
      add(d3);
      add(new Label("Salir"));
      add(ta);
      ta = new TextArea(10, 40);
   }
   
   public void actionPerformed(ActionEvent e){
      nc = d1.getText();
      nomb = d2.getText();
      clave = d3.getText();
      ta.setText("");
      ta.append("No Control"+nc);
      ta.append("Nombre"+nomb);
      ta.append("Clave"+clave);
   }
}