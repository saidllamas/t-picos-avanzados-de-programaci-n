import java.awt.*;
import java.applet.*;

public class EjemGUI7 extends Applet{
   public void init(){
//      setLayout(new FlowLayout(FlowLayout.LEFT, 50, 30));
      setLayout(new BorderLayout(50, 50));
      add(new Button("Soy un boton"), "West");
      add(new Choice(),"North");
      add(new Button("Soy otro boton"), "East");
      add(new Label("Soy una etiqueta"), "Center");
      add(new EjemGUI2(), "South");
      Panel p = new Panel();
      for(int i = 1; i <=10; i++){
        p.add(new Button("Boton"+i));
      }
      add(p, "Center");
   }
}