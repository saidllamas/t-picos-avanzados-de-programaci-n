import java.awt.*;

public class EjemGUI2 extends Panel{
	public EjemGUI2(){
		setLayout(new FlowLayout());
      add(new Button("Soy un boton"));
		add(new Choice());
		add(new Button("Soy otro boton"));
		add(new Label("Soy una etiqueta"));
	}
	public static void main(String[] args){
		Frame f = new Frame();
		f.setBounds(10,10,600,400);
	   f.add(new EjemGUI2());
		f.setVisible(true);
	}//main
}//class