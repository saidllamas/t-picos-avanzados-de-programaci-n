import java.awt.*;
import java.applet.*;

public class EjemGUI6 extends Applet{
   public void init(){
//      setLayout(new FlowLayout(FlowLayout.LEFT, 50, 30));
      FlowLayout fl = new FlowLayout();
      fl.setAlignment(FlowLayout.LEFT);
      fl.setHgap(30);
      fl.setVgap(30);
      setLayout(fl);
      add(new Button("Soy un boton"));
      add(new Choice());
      add(new Button("Soy otro boton"));
      add(new Label("Soy una etiqueta"));
//      add(new EjemGUI2());
      for(int i = 1; i <=10; i++){
         add(new Button("Boton"+i));
      }
   }
}