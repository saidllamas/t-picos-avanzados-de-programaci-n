import java.awt.*;
import java.applet.*;

public class EjemGUI9 extends Applet{

   public void init(){
      setLayout(new GridLayout(2, 3, 10, 10));
      add(new Button("Boton"));
      add(new Choice());
      add(new Button("Boton"));
      add(new Label("Etiqueta"));
      add(renglones());
      add(columnas());
   }   
   
   public static Panel renglones(){
      Panel p = new Panel(new GridLayout(10, 1));
      for(int i = 0; i < 10; i++) p.add(new Button("btn "+i));
      p.setVisible(true);
      return p;
   }
   
   public static Panel columnas(){
      Panel p = new Panel(new GridLayout(1,5));
      for(int i = 0; i < 5; i++) p.add(new Button("btn "+i));
      p.setVisible(true);
      return p;
   }
   
}