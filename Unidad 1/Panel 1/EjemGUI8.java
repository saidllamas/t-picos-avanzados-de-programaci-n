import java.awt.*;
import java.applet.*;

public class EjemGUI8 extends Applet{
   public void init(){
//      setLayout(new FlowLayout(FlowLayout.LEFT, 50, 30));
      setLayout(new GridLayout(4,4, 50, 50));
      add(new Button("Soy un boton"));
      add(new Choice());
      add(new Button("Soy otro boton"));
      add(new Label("Soy una etiqueta"));
      add(new EjemGUI2());
      Panel p = new Panel();
      for(int i = 1; i <=10; i++){
         add(new Button("Boton"+i));
      }
      add(p);
   }
}