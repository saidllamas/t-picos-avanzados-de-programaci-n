import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Boton1 extends Panel implements ActionListener{
   Button b, b2, b3, b4;
   
   public Boton1(){
      b = new Button("boton 1");
      b2 = new Button("boton 2");
      add(b);
      add(b2);
      b.addActionListener(this);
      b2.addActionListener(this);
      b3 = new Button("Cerrar");
      b3.addActionListener(this);
      add(b3);
      b4 = new Button("Circulo");
      b4.addActionListener(this);
      add(b4);
   }
   
   public static void main(String[] args){
      Frame f = new Frame();
		f.setBounds(10,10,600,400);
	   f.add(new Boton1());
		f.setVisible(true);
/*      f.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
      */
      f.addWindowListener(new Cerrar());
//      f.addWindowListener(new Cerrar2());
   }
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == b){
         System.out.println("b1");
         setBackground(new Color((float)Math.random(), (float)Math.random(), (float)Math.random(), (float)Math.random()));
      }else if(e.getSource() == b3){
         System.exit(0);
      }else if(e.getSource() == b4){
         Frame fc = new Frame();
         fc.setBounds(100,200,100,100);
         fc.setVisible(true);
         fc.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               fc.setVisible(false);
               fc.dispose();
              // fc = null;
            }
         }
         );
         Circulos c = new Circulos();
         c.init();
         c.start();
         fc.add(c);
      }else{
         System.out.println("b2");
         b.setForeground(new Color((float)Math.random(), (float)Math.random(),(float)Math.random(), (float)Math.random()));
      }
   }
   
}