import java.applet.*;
import java.awt.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: 
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Changuto extends Applet{

   public void init(){

   }//end construct
   
   public void paint(Graphics g){
      g.drawOval(100,100,120,120);//cara
      g.drawOval(95,85,40,40);
      g.drawOval(100,90,20,20);
      g.drawOval(190,85,40,40);
      g.drawOval(200,94,20,20);
//      g.drawArc(120,110,120,110,0,-180);
      g.setColor(Color.WHITE);
      g.fillOval(101,101,118,118);//cara FONDO
      g.setColor(Color.BLACK);//Ojos
      g.fillOval(135,140,10,20); //izq
      g.fillOval(180,140,10,20); //der
      g.drawArc(100,110,120,130,0,180);
      g.drawOval(160,160,10,10); //nariz
      g.drawArc(135,160,50,40,0,-180);
   }
   
}//end class