import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: 
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Interfaz extends Panel{

   Button btnRegistrar, btnCancelar;
   TextField tfC, tfN, tfE, tfPro, tfTel;
   
   public Interfaz(){

      setLayout(null);
      
      btnRegistrar = new Button("Registrar");
      btnRegistrar.setBounds(100, 240, 110,23);
      btnCancelar = new Button("Cancelar");
      btnCancelar.setBounds(320, 240, 110,23);
      tfC = new TextField();
      tfC.setBounds(80, 80, 110,23);
      tfN = new TextField();
      tfN.setBounds(80, 130, 210,23);
      tfPro = new TextField();
      tfPro.setBounds(80, 180, 210,23);
      tfE = new TextField();
      tfE.setBounds(410, 130, 90,23);
      tfTel = new TextField();
      tfTel.setBounds(410, 180, 90,23);
      
      Label l = new Label("REGISTRO DE PERSONAS");
      l.setFont(new Font("SanSerif", Font.BOLD, 28));
      l.setBounds(120,20,400,25);
      add(l);
      Label a = new Label("Codigo");
      a.setBounds(20,80,45,25);
      add(a);
      Label b = new Label("Nombre");
      b.setBounds(20,127,49,25);
      add(b);
      Label c = new Label("Profesion");
      c.setBounds(20,180,54,25);
      add(c);
      Label d = new Label("Edad");
      d.setBounds(350,127,49,25);
      add(d);
      Label e = new Label("Telefono");
      e.setBounds(350,180,54,25);
      add(e);
      
      add(btnRegistrar);
      add(btnCancelar);
      add(tfN);
      add(tfC);
      add(tfE);
      add(tfPro);
      add(tfTel);
      
   }//end construct
   
   /*
   public static void main(String[] args){
      Interfaz i = new Interfaz();
      Frame f = new Frame();
      f.add(i);
      f.setBounds(10,10,600,320);
      f.setVisible(true);
      f.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }*/
}//end class