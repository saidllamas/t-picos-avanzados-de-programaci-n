import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: 
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Bienvenida extends Frame implements ActionListener{

   Button btnConversiones, btnBienvenida, btnChanguito, btnInterfaz, btnSalir;
   
   public Bienvenida(){

      setLayout(null);
      
      btnConversiones = new Button("Conversiones");
      btnConversiones.setBounds(20, 50, 110,23);
      btnBienvenida = new Button("Bienvenida");
      btnBienvenida.setBounds(20, 75, 110,23);
      btnChanguito = new Button("Changuito");
      btnChanguito.setBounds(20, 100, 110,23);
      btnInterfaz = new Button("Interfaz");
      btnInterfaz.setBounds(20, 125, 110,23);
      btnSalir = new Button("Salir");
      btnSalir.setBounds(20, 175, 110,23);
      
      btnConversiones.addActionListener(this);
      btnBienvenida.addActionListener(this);
      btnChanguito.addActionListener(this);
      btnInterfaz.addActionListener(this);
      btnSalir.addActionListener(this);
      
      add(btnConversiones);
      add(btnBienvenida);
      add(btnChanguito);
      add(btnInterfaz);
      add(btnSalir);
      
   }//end construct
   
   public void actionPerformed(ActionEvent a){
      
   }//end actionPerformed
   
}//end class