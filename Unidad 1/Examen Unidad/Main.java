import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: 
*/

public class Main extends Frame implements ActionListener{

   Button btnConversiones, btnBienvenida, btnChanguito, btnInterfaz, btnSalir;
   
   public Main(){

      setLayout(null);
      
      btnConversiones = new Button("Conversiones");
      btnConversiones.setBounds(20, 50, 110,23);
      btnBienvenida = new Button("Bienvenida");
      btnBienvenida.setBounds(20, 75, 110,23);
      btnChanguito = new Button("Changuito");
      btnChanguito.setBounds(20, 100, 110,23);
      btnInterfaz = new Button("Interfaz");
      btnInterfaz.setBounds(20, 125, 110,23);
      btnSalir = new Button("Salir");
      btnSalir.setBounds(20, 150, 110,23);
      
      btnConversiones.addActionListener(this);
      btnBienvenida.addActionListener(this);
      btnChanguito.addActionListener(this);
      btnInterfaz.addActionListener(this);
      btnSalir.addActionListener(this);
      
      add(btnConversiones);
      add(btnBienvenida);
      add(btnChanguito);
      add(btnInterfaz);
      add(btnSalir);
      
   }//end construct
   
   public void actionPerformed(ActionEvent a){
      if(a.getSource() == btnSalir){
         JOptionPane.showMessageDialog(this, "Informacion general \n Jesus Said Llamas Manriquez \n15290902\n4 Semestre\nIngenieria en Sistemas\nM-11");
         System.exit(0);
      }
      else if(a.getSource() == btnBienvenida){
         /*
         Bienvenida bva = new Bienvenida();
         bva.setBounds(10,20,1000, 700);
         bva.setResizable(false);
         bva.setVisible(true);
         bva.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               bva.setVisible(false);
            }
         });*/
      }else if(a.getSource() == btnChanguito){
         try{
            Runtime.getRuntime().exec("appletviewer changuito.html");
         }catch(Exception e){
            System.out.println(e);
         }
      }else if(a.getSource() == btnInterfaz){
         Interfaz i = new Interfaz();
         Frame f = new Frame();
         f.add(i);
         f.setBounds(10,10,600,320);
         f.setVisible(true);
         f.setResizable(false);
         f.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               f.setVisible(false);
            }
         });
      }else if(a.getSource() == btnConversiones){
         Conversiones c = new Conversiones();
         c.setBounds(10,10,400,250);
         c.setVisible(true);
         c.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               System.exit(0);
            }
         });
      }
   }//end actionPerformed
   
   public static void main(String[] args){
     Main app = new Main();
     app.setBounds(100,30,160, 200);
     app.setResizable(false);
     app.setVisible(true);
     app.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
         System.exit(0);
      }
     });
   }//end main
}//end class