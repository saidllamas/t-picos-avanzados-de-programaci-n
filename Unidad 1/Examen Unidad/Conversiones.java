import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: 
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Conversiones extends Frame implements ActionListener{

   Button btnBy, btnKb, btnMb, btnGb, btnSalir;
   TextField tfVal, tfRes;
   
   public Conversiones(){

      setLayout(null);
      
      tfVal = new TextField();
      tfVal.setBounds(20, 40, 330,20);
      tfRes = new TextField();
      tfRes.setBounds(20, 135, 330,40);
      tfRes.setEditable(false);
      btnBy = new Button("A Bytes");
      btnBy.setBounds(20, 70, 110,23);
      btnKb = new Button("A Kilobytes");
      btnKb.setBounds(20, 95, 110,23);
      btnMb = new Button("A Megabytes");
      btnMb.setBounds(150, 70, 110,23);
      btnGb = new Button("A Gigabytes");
      btnGb.setBounds(150, 95, 110,23);
      btnSalir = new Button("Salir");
      btnSalir.setBounds(20, 185, 310,23);
      
      btnBy.addActionListener(this);
      btnKb.addActionListener(this);
      btnMb.addActionListener(this);
      btnGb.addActionListener(this);
      btnSalir.addActionListener(this);
      
      add(tfVal);
      add(tfRes);
      add(btnBy);
      add(btnKb);
      add(btnMb);
      add(btnGb);
      add(btnSalir);
      
   }//end construct
   
   public void actionPerformed(ActionEvent a){
      double by = Double.parseDouble(tfVal.getText());
      double kb = 0;
      double mb = 0;
      double gb = 0;
      if(a.getSource() == btnBy){
         tfRes.setText(""+by/8);
      }else if(a.getSource() == btnKb){
         tfRes.setText(""+(by/8)/1024);
      }else if(a.getSource() == btnMb){
         tfRes.setText(""+((by/8)/1024)/1042);
      }else if(a.getSource() == btnGb){
         double r = (((by/8)/1024)/1042)/1024;
         tfRes.setText(""+r);
      }else if(a.getSource() == btnSalir)setVisible(false);
      
   }//end actionPerformed
   
   /*
   public static void main(String[] args){
      Conversiones c = new Conversiones();
      c.setBounds(10,10,400,250);
      c.setVisible(true);
      c.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }*/
   
   
}//end class