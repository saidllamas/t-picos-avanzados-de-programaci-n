import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Cuadritos2 extends Applet{
   public Cuadritos2(){
      repaint();
   }
   
   public void paint(Graphics g){
      for(int i=1; i<=10; i++){
         g.setColor(new Color((float)Math.random(), .5f, .3f));
         g.fillRect(10, i*20, 100, 100);
      }//for
   }//paint
   
   public static void main(String[] args){
      Frame f = new Frame();
     f.addWindowListener(new java.awt.event.WindowAdapter() {
          public void windowClosing(java.awt.event.WindowEvent e) {
          System.exit(0);
          };
        });
        f.pack();
     f.setSize(100,100 + 20);
     f.show();
   }
}//class