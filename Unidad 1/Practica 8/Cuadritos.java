import java.awt.*;
import java.awt.event.*;

public class Cuadritos extends Panel{
   public Cuadritos(){
      repaint();
   }
   
   public void paint(Graphics g){
      for(int i=1; i<=10; i++){
         g.setColor(new Color((float)Math.random(), .5f, .3f));
         g.fillRect(10, i*20, 100, 100);
      }//for
   }//paint
   
    public static void main(String[] args){
      Frame f = new Frame();
      f.add(new Cuadritos());
      f.setVisible(true);
      f.setSize(150, 350);
      
      f.addWindowListener(new WindowAdapter(){     
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }
}//class