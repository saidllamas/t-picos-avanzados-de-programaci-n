import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Emergentes5 extends Frame implements ActionListener{
   Frame f,g;
   PopupMenu m1, m2;
   MenuItem op1, op2, op3, op4;
   
   public Emergentes5(){
      setLayout(null);
      m1 = new PopupMenu("Menu 1");
      m2 = new PopupMenu("Menu 2");
      op1 = new MenuItem("Opcion 1");
      op2 = new MenuItem("Opcion 2");
      op3 = new MenuItem("Salir", new MenuShortcut(KeyEvent.VK_X));
      op4 = new MenuItem("Opcion 4");
      
      m1.add(op1);
      m1.addSeparator();
      m1.add(op3);
      m1.add(op4);
      m2.add(op2);
      add(m1);
      add(m2);
  
      setFont(new Font("",0,24));
      Label l = new Label("Ejemplo De Un Menu Emergente");
      l.setBounds(30,50,380,30);
      add(l);
      op1.addActionListener(this);
      op2.addActionListener(this);
      op3.addActionListener(this);
      op4.addActionListener(this);
      addMouseListener(new Posicion(this, m1));
      addMouseListener(new Posicion(this, m2));
      setSize(500,500);
      setVisible(true);   
   }//Emergente
   
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == op1){
         g = new Frame();
         g.add(new Cuadritos2());
         g.setVisible(true);
         g.setSize(150, 350);
         
         g.addWindowListener(new WindowAdapter(){     
            public void windowClosing(WindowEvent e){
               g.setVisible(false);
            }
         });
      }else if(e.getSource() == op2){
         f = new Frame();
         f.add(new Cuadritos2());
         f.setVisible(true);
         f.setSize(150, 350);
         
         f.addWindowListener(new WindowAdapter(){     
            public void windowClosing(WindowEvent e){
               f.setVisible(false);
            }
         });
         //JOptionPane.showMessageDialog(this, "Soy La Opcion 2");
         }else if(e.getSource() == op3){
            JOptionPane.showMessageDialog(this, "Hasta Luego");
            System.exit(0);
         }else if(e.getSource() == op4){
            try{Runtime.getRuntime().exec("java Cuadritos");
            }catch(Exception s){}
         }
   }//actionPerformed
   
   
   public static void main(String[] args){
      Emergentes5 app = new Emergentes5();
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
      
   }//main
   
   
   class Posicion extends MouseAdapter{
      Frame f;
      PopupMenu p;
      
      public Posicion(Frame f, PopupMenu p){
         this.f = f;
         this.p = p;
      }//Posicion
      
      
      public void mousePressed(MouseEvent e){
         if((p.getLabel().equals("Menu 1") && (e.getButton() == MouseEvent.BUTTON1)) || (p.getLabel().equals("Menu 2") && (e.getButton() == MouseEvent.BUTTON3))){
            p.show(f, e.getX(), e.getY());
         }//if
      }//mousePressed
   }//Posicion
}//class