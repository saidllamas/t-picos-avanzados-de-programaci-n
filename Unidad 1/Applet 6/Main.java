import java.applet.*;
import java.awt.*;

public class Main extends Applet{
	String nom, edad;
	public void init(){
		nom = getParameter("Nombre");
		edad = getParameter("Edad");
		setBackground(Color.GRAY);
	}//end init

	public void paint(Graphics g){
		g.setFont(new Font("Courier", Font.BOLD + Font.ITALIC, 30));
		String msj = "Hola "+nom+ " tu edad es "+edad;
		g.drawString(msj, 50, 75);
	}//end paint
}//end class