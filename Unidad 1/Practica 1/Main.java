/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 02/02/2017 como Practica #1
*	para la materia de topicos avanzados de programacion.
*	El siguiente construye 5 diferentes matrices (de diferentes tamaños) y las rellena con un patron (diferente)
*	en cada una, generando numeros con incremento de +1.
*	Para apoyarme por la complejidad de los arreglos 3, 4 y 5 decidi usar metodos para el llenado de cada uno
*	al final solo tengo un metodo que imprime todos los arreglos, dejando un pequeño espacio e imprimiendo | al
*	finalizar la impresion de la casilla.
*	*NOTA: El programa esta pensdo en seguir funcionando a pesar de cambiar el tamaño de las variables.*
* 	Link del repositorio de la materia: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/
*/

public class Main{
	//Arreglos a usar.
	static int[][] arreglo1 = new int[10][5], arreglo2 = new int[5][10], arreglo3 = new int[10][8], arreglo4 = new int[10][10], arreglo5 = new int[10][10];

	public static void main(String[] args) {
		llenado(); //generar las matrices
		imprimir(); //imprimir las matrices generadas
	}//end main

	public static void llenado(){
		//Arreglo 1 10x5
		int c = 1;
		for(int i = 0; i < 10; i++){
			if((i % 2) == 0){
				for (int x = 0; x < 5; x++) {
					arreglo1[i][x] = c++;
				}//--for
			}//if
		}//for

		//Arreglo 2 5x10
		c = 1; //reinicio el contador
		for (int i = 0; i < 10; i++) {
			if((i % 2) != 0){
				for (int x = 0; x < 5 ; x++) {
					arreglo2[x][i] = c++;
				}//--for
			}//if
		}//for

		//Arreglo 3 10x8
		c = 1; //reinicio el contador
		llenarZigZag(c);

		//Arreglo 4 10x10
		c = 1;
		llenarEspiral(c);

		//Arreglo 5 10x10
		c = 1;		
		llenarTrazado(c);
	}//end llenado

	public static void llenarZigZag(int c){ //Metodo exclusivo para el llenado del arreglo #3
		boolean haciaDerecha = true; //de izq a der
		for (int w = 0; w < 10; w+=2) { //Renglones a saltar
			if(haciaDerecha){
				for (int i = 0;  i < 8; i++) {
					arreglo3[w][i] = c++;
				}
				haciaDerecha = false; //cambia el signo
			}else{
				for (int i = 7;  i >= 0; i--) {
					arreglo3[w][i] = c++;
				}
				haciaDerecha = true; //cambia el signo
			}//else
		}//for
	}//end llenarZigZag

	public static void llenarEspiral(int c){ //Metodo exclusivo para el llenado del arreglo #4
		for (int i = 0; i < 10; i++) {
			for (int x = i; x < 10 - i; x++) {	//izq-der
                arreglo4[i][x] = c++;
            }//for
            for (int x = i; x < 10 - i; x++) {	//arr-aba
                arreglo4[x][9 - i] = c++;
            }//for
            for (int x = 9 - i; x > i ; x--) {  //der-izq
                arreglo4[9 - i][x] = c++;
            }//for
            for (int x = 9 - i; x > i; x--) {	//aba-arr
               	arreglo4[x][i] = c++;
            }//for
		}//for
	}//end llenarEspiral

	public static void llenarTrazado(int c){ //Metodo exclusivo para el llenado del arreglo #5
		for (int i = 9; i >= 0; i--) { //diagonal der-izq -subida
			for (int x = 9; x >= 0; i--) {
				arreglo5[i][x] = c++;
				x--; //-1 a la izq
			}//--for
			i--; //-1 a altura
		}//for

		for (int i = 1; i < 10; i++) { //izq-der
			arreglo5[0][i] = c++; //renglon superior
		}//for

		for (int i = 9; i >= 0; i--) { //diagonal izq-der -bajada
			for (int x = 0; x < 10; x++ ) {
				arreglo5[x][i] = c++;
				i--; //-1 altura
			}//--for
		}//for	
	}//end llenarZigZag

	public static void imprimir(){
		//Arreglo 1
		System.out.println("	Arreglo #1... Salto de un renglon");
		for (int i = 0; i < 10; i++) {
			System.out.print("  "); //
			for (int x = 0; x < 5; x++) {
				System.out.print(arreglo1[i][x]+"  | ");
			}//--for
			System.out.println(); //salto de renglon
		}//for

		System.out.println(""); //salto entre arreglos 

		//Arreglo 2
		System.out.println("	Arreglo #2... Salto de una columna");
		for (int i = 0; i < 5; i++) {
			System.out.print("  "); //
			for (int x = 0; x < 10; x++) {
				System.out.print(arreglo2[i][x]+"  | ");
			}//--for
			System.out.println(); //salto de renglon
		}//for

		System.out.println(""); //salto entre arreglos 

		//Arreglo 3
		System.out.println("	Arreglo #3... ZigZag");
		for (int i = 0; i < 10; i++) {
			System.out.print("  "); //
			for (int x = 0; x < 8; x++) {
				System.out.print(arreglo3[i][x]+"  | ");
			}//--for
			System.out.println(); //salto de renglon
		}//for

		System.out.println(""); //salto entre arreglos 

		//Arreglo 4
		System.out.println("	Arreglo #4... Espiral");
		for (int i = 0; i < 10; i++) {
			System.out.print("  "); //
			for (int x = 0; x < 10; x++) {
				System.out.print(arreglo4[i][x]+"  | ");
			}//--for
			System.out.println(); //salto de renglon
		}//for

		System.out.println(""); //salto entre arreglos 

		//Arreglo 5
		System.out.println("	Arreglo #5... Trazado");
		for (int i = 0; i < 10; i++) {
			System.out.print("  "); //
			for (int x = 0; x < 10; x++) {
				System.out.print(arreglo5[i][x]+"  | ");
			}//--for
			System.out.println(); //salto de renglon
		}//for
		System.out.println("_________________ end ________________");
	}//end imprimir

}//end class