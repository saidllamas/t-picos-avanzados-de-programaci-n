import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Imitar una interfaz proporcionada y darle funcionalidad a los botones
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class Practica9 extends Frame implements ActionListener{
   
   Button btnFiguras, btnCancion, btnBlocNts, btnBarraMenu, btnMenuEmg, btnSalir, btnAcercaD, btnInfoGn;
   MenuItem miFiguras, miCancion, miBlocNts, miBarraMenu, miMenuEmg, miSalir, miAcercaD;
   MenuItem sbmiInfoFig, sbmiInfoCanc, sbmiBarraMenu, sbmiMenuEmg, sbmiInfoFondo;
   public Practica9(){
      MenuBar menuBar = new MenuBar();
      Menu mnActividades = new Menu("Actividades");
      Menu mnInformacion = new Menu("Infomacion");
      Menu sbmnAyuda = new Menu("Ayuda");
      
      setLayout(null);
      
      btnFiguras = new Button("Figuras");
      btnFiguras.setBounds(20, 50, 110,23);
      btnCancion = new Button("Cancion");
      btnCancion.setBounds(20, 75, 110,23);
      btnBlocNts = new Button("Bloc de notas");
      btnBlocNts.setBounds(20, 100, 110,23);
      btnBarraMenu = new Button("Barra de menus");
      btnBarraMenu.setBounds(20, 125, 110,23);
      btnMenuEmg = new Button("Menu Emergente");
      btnMenuEmg.setBounds(20, 150, 110,23);
      btnSalir = new Button("Salir");
      btnSalir.setBounds(20, 175, 110,23);
      btnAcercaD = new Button("Acerca de");
      btnAcercaD.setBounds(670, 50, 115, 23);
      btnInfoGn = new Button("Informacion general");
      btnInfoGn.setBounds(670, 75, 115, 23);
      
      miFiguras = new MenuItem("Figuras");
      miCancion = new MenuItem("Cancion");
      miBlocNts = new MenuItem("Bloc de notas");
      miBarraMenu = new MenuItem("Barra de menus");
      miMenuEmg = new MenuItem("Menu Emergente");
      miSalir = new MenuItem("Salir", new MenuShortcut(KeyEvent.VK_S));
      
      mnActividades.add(miFiguras);
      mnActividades.add(miCancion);
      mnActividades.add(miBlocNts);
      mnActividades.add(miBarraMenu);
      mnActividades.add(miMenuEmg);
      mnActividades.add(miSalir);
      
      miAcercaD = new MenuItem("Acerca de");
      sbmiInfoFig = new MenuItem("InfoFiguras");
      sbmiInfoCanc = new MenuItem("InfoCancion");
      sbmiBarraMenu = new MenuItem("Barra de menus");
      sbmiMenuEmg = new MenuItem("Menu Emergente");
      sbmiInfoFondo = new MenuItem("InfoFondo");
      mnInformacion.add(miAcercaD);
      sbmnAyuda.add(sbmiInfoFig);
      sbmnAyuda.add(sbmiInfoCanc);
      sbmnAyuda.add(sbmiBarraMenu);
      sbmnAyuda.add(sbmiInfoFondo);
      mnInformacion.add(sbmnAyuda);
      
      
      btnFiguras.addActionListener(this);
      btnCancion.addActionListener(this);
      btnBlocNts.addActionListener(this);
      btnBarraMenu.addActionListener(this);
      btnMenuEmg.addActionListener(this);
      btnSalir.addActionListener(this);
      btnAcercaD.addActionListener(this);
      btnInfoGn.addActionListener(this);
      miFiguras.addActionListener(this);
      miCancion.addActionListener(this);
      miBlocNts.addActionListener(this);
      miBarraMenu.addActionListener(this);
      miMenuEmg.addActionListener(this);
      miSalir.addActionListener(this);
      miAcercaD.addActionListener(this);
      sbmiInfoFig.addActionListener(this);
      sbmiInfoCanc.addActionListener(this);
      sbmiBarraMenu.addActionListener(this);
      sbmiMenuEmg.addActionListener(this);
      sbmiInfoFondo.addActionListener(this);
      
      add(btnFiguras);
      add(btnCancion);
      add(btnBlocNts);
      add(btnBarraMenu);
      add(btnMenuEmg);
      add(btnSalir);
      add(btnAcercaD);
      add(btnInfoGn);
      
      menuBar.add(mnActividades);
      menuBar.add(mnInformacion);
      
      setMenuBar(menuBar);
   }//end construct
   
   public void actionPerformed(ActionEvent a){
      if(a.getSource() == btnSalir || a.getSource() == miSalir) System.exit(0);
      else if(a.getSource() == btnFiguras || a.getSource() == miFiguras){
         Figuras figs = new Figuras();
         figs.setBounds(10,20,1000, 700);
         figs.setResizable(false);
         figs.setVisible(true);
         figs.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               figs.setVisible(false);
            }
         });
      }else if(a.getSource() == btnCancion || a.getSource() == miCancion){
         try{
            String [] cmd = {"shutdown","-s","-t", "10"}; //comando de apagado en windows
            //Runtime.getRuntime().exec(" cmd /c start c:\\Almacenamiento\\java.mp3");
            Runtime.getRuntime().exec(" cmd /c start musica.mp3");
            //Process shutdown = Runtime.getRuntime() .exec(cmd); //Comando interesante
         }catch(Exception e){
            System.out.println(e);
         }
      }else if(a.getSource() == btnBlocNts || a.getSource() == miBlocNts){
         try{
            Runtime.getRuntime().exec("notepad.exe");
         }catch(Exception e){
            System.out.println(e);
         }
      }else if(a.getSource() == btnBarraMenu || a.getSource() == miBarraMenu){
         BarraMenu mb = new BarraMenu();
         mb.setBounds(10,20,100, 100);
         mb.setResizable(false);
         mb.setVisible(true);
         mb.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               mb.setVisible(false);
            }
         });
      }else if(a.getSource() == btnMenuEmg || a.getSource() == miMenuEmg){
         MenuEmergente me = new MenuEmergente();
         me.setBounds(10,20,700, 500);
         me.setResizable(false);
         me.setVisible(true);
         me.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               me.setVisible(false);
            }
         });
      }else if(a.getSource() == btnAcercaD || a.getSource() == miAcercaD){
         JOptionPane.showMessageDialog(this, "Practica 9");
      }else if(a.getSource() == btnInfoGn){
         JOptionPane.showMessageDialog(this, "Informacion general \n Este programa hace muchas operaciones");
      }
   }//end actionPerformed
   
   public static void main(String[] args){
     Practica9 app = new Practica9();
     app.setBounds(100,30,800, 500);
     app.setResizable(false);
     app.setVisible(true);
     app.addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent e){
            System.exit(0);
      }
     });
   }//end main
}//end class