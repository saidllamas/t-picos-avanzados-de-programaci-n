import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Clase de apoyo, codigo tomado de ejemplos realizados en clases
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class BarraMenu extends Frame implements ActionListener{
   MenuItem op1, op2, op3, op4, op5, salir;
   String cad1, cad2;
   CheckboxMenuItem op6, op7;
   
   public BarraMenu(){
      MenuBar barra = new MenuBar();
      Menu menu1 = new Menu("Menu");
      Menu menu2 = new Menu("Menu2");
      Menu submenu = new Menu("Submenu");
      op1 = new MenuItem("Opcion 1");
      op2 = new MenuItem("Opcion 2");
      op3 = new MenuItem("Opcion 3");
      op4 = new MenuItem("Opcion 4");
      op5 = new MenuItem("Opcion 5");
      op6 = new CheckboxMenuItem("Opcionn 6");
      op7 = new CheckboxMenuItem("Opcionn 7");
      salir = new MenuItem("Salir", new MenuShortcut(KeyEvent.VK_X));
      menu1.add(op1);
      menu1.add(op2);
      menu1.addSeparator();
      menu1.add(op3);
      submenu.add(op4);
      submenu.add(op5);
      menu1.add(submenu);
      menu1.addSeparator();
      menu1.add(salir);
      barra.add(menu1);
      barra.add(menu2);
      menu2.add(op6);
      menu2.add(op7);
      
      op1.addActionListener(this);
      op2.addActionListener(this);
      op3.addActionListener(this);
      op4.addActionListener(this);
      op5.addActionListener(this);
      salir.addActionListener(this);
      setMenuBar(barra);
      setBounds(0,0,100,80);
      setVisible(true);
   }//construct
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == op1){
         JOptionPane.showMessageDialog(null, "Soy la opcion 1");
      }else if(e.getSource() == op2){
         JOptionPane.showMessageDialog(this, "Soy la opcion 2");
      }else if(e.getSource() == op3){
         JOptionPane.showMessageDialog(null, "Soy la opcion 3");
      }else if(e.getSource() == salir){
         setVisible(false);
      }else if(e.getSource() == op4){
         try{
            Runtime.getRuntime().exec("notepad.exe");
         }catch(Exception ei){}
      }
   }//actionPerformed
     
}//class
