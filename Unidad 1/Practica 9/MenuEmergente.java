import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*  @autor: J. Said Llamas Manriquez
*  @materia: Topicos Avanzados de Programacion
*  @descripcion: Clase de apoyo, codigo tomado de ejemplos realizados en clases
*  @link: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/src
*/

public class MenuEmergente extends Frame implements ActionListener{
	PopupMenu m1, m2;
	MenuItem op1, op2, op3;

	public MenuEmergente(){
		setLayout(new FlowLayout());

		m1 = new PopupMenu("Menu 1");
		m2 = new PopupMenu("Menu 2");
		op1 = new MenuItem("Opcion 1");
		op2 = new MenuItem("Opcion 2");
		op3 = new MenuItem("Salir");

		m1.add(op1);
		m1.addSeparator();
		m1.add(op3);
		m2.add(op2);

		add(m1);
		add(m2);

		op1.addActionListener(this);
		op2.addActionListener(this);
		op3.addActionListener(this);

		addMouseListener(new Posicion(this, m1));
		addMouseListener(new Posicion(this, m2));

		setSize(500, 500);
		setVisible(true);
	}//end constructor

	public void actionPerformed(ActionEvent e){
      if(e.getSource() == op1)JOptionPane.showMessageDialog(this, "Soy la opcion 1");
      else if(e.getSource() == op2)JOptionPane.showMessageDialog(this, "Soy la opcion 2");
      else if(e.getSource() == op3){
         JOptionPane.showMessageDialog(this, "Hasta luego");
         setVisible(false);
      }
	}
}//end class

class Posicion extends MouseAdapter{
	Frame f;
	PopupMenu p;

	public Posicion(Frame f, PopupMenu p){
		this.f = f;
		this.p = p;
	}

	public void mousePressed(MouseEvent e){
      if(p.getLabel().equals("Menu 1") && e.getButton() == MouseEvent.BUTTON3) p.show(f, e.getX(), e.getY());
      else if(p.getLabel().equals("Menu 2") && e.getButton() == MouseEvent.BUTTON1) p.show(f, e.getX(), e.getY());
	}
}