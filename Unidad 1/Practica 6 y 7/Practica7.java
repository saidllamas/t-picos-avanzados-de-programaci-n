import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;

/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 02/04/2017 como Practica #7
*	para la materia de topicos avanzados de programacion.
*	El siguiente construye un frame que manda a llamar a dos frames, uno simula una agenda y el otro
*  un registro de horarios.
* 	Link del repositorio de la materia: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/
*/

public class Practica7 extends Frame implements ActionListener{
   
   MenuItem fr1, fr2, salir, aplicacion, desarrollador;
   Button btnFr1, btnFr2, btnSalir;
   Menu menu1, menu2;
   
   public Practica7(){
      setLayout(null);//desactivo el gestor de layout
      
      //construyendo elementos
      btnFr1 = new Button("Frame 1");
      btnFr2 = new Button("Frame 2");
      
      MenuBar barra = new MenuBar();
      
      menu1 = new Menu("Men�");
      menu2 = new Menu("Acerca de");
      
      fr1 = new MenuItem("Frame 1");
      fr2 = new MenuItem("Frame 2");
      aplicacion = new MenuItem("Aplicaci�n");
      desarrollador = new MenuItem("Desarrollador");
      salir = new MenuItem("Salir", new MenuShortcut(KeyEvent.VK_X));
      
      //ajustando elementos
      btnFr1.setBounds(150,200, 100,25);
      btnFr2.setBounds(310,200, 100,25);
      
      //escuchadores      
      fr1.addActionListener(this);
      fr2.addActionListener(this);
      aplicacion.addActionListener(this);
      desarrollador.addActionListener(this);
      salir.addActionListener(this);
      btnFr1.addActionListener(this);
      btnFr2.addActionListener(this);
      
      //a�adiendo al contenedor
      menu1.add(fr1);
      menu1.add(fr2);
      menu1.add(salir);
      menu2.add(aplicacion);
      menu2.add(desarrollador);
      barra.add(menu1);
      barra.add(menu2);
      setMenuBar(barra);
      add(btnFr1);
      add(btnFr2);
   }//constuctor
   
   public void paint(Graphics g){
      g.setFont(new Font("Arial", 0, 25)); //fuente para el titulo
      g.drawString("Practica #1",200,80);  //Titulo de app

      g.setFont(new Font("Arial", 0, 22)); //fuente para el creador
      g.drawString("Jes�s Said Llamas Manriquez", 140, 575); //Creditos
      
      //reset color
      g.setColor(Color.BLACK);
      
   }//paint
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == fr1){ //Frame 1
         Frame1 frame = new Frame1();
         frame.setBounds(10,360,400,140);
         frame.setVisible(true);
         frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               frame.setVisible(false);
            }
         });
      }else if(e.getSource() == fr2){ // Frame 2
         Frame2 frame = new Frame2();
         frame.setBounds(10,50,400,300);
         frame.setVisible(true);
         frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               frame.setVisible(false);
            }
         });
      }else if(e.getSource() == aplicacion){ //Acerca de- aplicacion
      JOptionPane.showMessageDialog(null,"El siguiente construye un frame que manda a llamar a dos frames, uno simula una agenda y el otro  un registro de horarios.");
      }else if(e.getSource() == desarrollador){ //Acerca de- desarrollador
      JOptionPane.showMessageDialog(null,"Jesus Said Llamas Manriquez");
      }else if(e.getSource() == btnFr1){
         Frame1 frame = new Frame1();
         frame.setBounds(10,360,400,140);
         frame.setVisible(true);
         frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               frame.setVisible(false);
            }
         });
      }else if(e.getSource() == btnFr2){
         Frame2 frame = new Frame2();
         frame.setBounds(10,50, 400,300);
         frame.setVisible(true);
         frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               frame.setVisible(false);
            }
         });
      }else if(e.getSource() == salir){ //salir
         System.exit(0);
      }
   }//actionPerformed
   
   public static void main(String[] args){
      Practica7 app = new Practica7();
      app.setBounds(460, 40, 550, 600); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tama�o
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }//main
}//class

class Frame1 extends Frame implements ActionListener{
   Button btnGuardar, btnMostrar, btnSalir;
   TextField txtMateria, txtHora;
   Choice chDia;
   String log = "";
   public Frame1(){
      setLayout(null);
      setResizable(false); //bloqueo ajuste de tama�o
      
      btnGuardar = new Button("Guardar datos");
      btnMostrar = new Button("Mostrar datos");
      btnSalir = new Button("Salir");
      txtMateria = new TextField();
      txtHora = new TextField();
      chDia = new Choice();
      
      
      chDia.add("Lunes");
      chDia.add("Martes");
      chDia.add("Miercoles");
      chDia.add("Jueves");
      chDia.add("Viernes");
      chDia.add("Sabado");
      chDia.add("Domingo");
      
      chDia.setBounds(140,60, 100,22);
      btnGuardar.setBounds(20,110,100,22);
      btnGuardar.addActionListener(this);
      btnMostrar.setBounds(130,110,100,22);
      btnMostrar.addActionListener(this);
      btnSalir.setBounds(240,110,100,22);
      btnSalir.addActionListener(this);
      txtMateria.setBounds(20, 60, 100, 22);
      txtHora.setBounds(260, 60, 100,22);
      
      
      add(chDia);
      add(txtMateria);
      add(txtHora);
      add(btnGuardar);
      add(btnMostrar);
      add(btnSalir);
   }//construct
   
   public void paint(Graphics g){
      g.drawString("Materia: ", 20,50);
      g.drawString("Dia", 140, 50);
      g.drawString("Hora inicio", 260, 50);
   }//paint
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == btnSalir){
         this.setVisible(false);
      }else if(e.getSource() == btnGuardar){
         log += txtMateria.getText() + " "+ txtHora.getText()+"\n";
         JOptionPane.showMessageDialog(null,"Registrado correctamente.");
         txtMateria.setText("");
         txtHora.setText("");
         chDia.select(0);
      }else if(e.getSource() == btnMostrar){
         JOptionPane.showMessageDialog(null,log);
      }
   }//actionPerfomed
   
}//class Frame1

class Frame2 extends Frame implements ActionListener{
   TextField txtNombre, txtTelefono;
   Choice chTipoTl;
   Button btnGuardar, btnSalir;
   TextArea log;
   
   public Frame2(){
      setLayout(null);
      setResizable(false); //bloqueo ajuste de tama�o
      
      txtNombre = new TextField();
      txtTelefono = new TextField();
      btnGuardar = new Button("Registrar datos");
      chTipoTl = new Choice();
      log = new TextArea();
      
      
      chTipoTl.add("Personal");
      chTipoTl.add("Casa");
      chTipoTl.add("Trabajo");
      
      
      txtNombre.setBounds(20, 60, 100, 22);
      txtTelefono.setBounds(270, 60, 100, 22);
      btnGuardar.setBounds(150, 100, 100, 22);
      chTipoTl.setBounds(140, 60, 100, 22);
      log.setBounds(20, 130, 335, 130);
      log.setEditable(false);
      
      btnGuardar.addActionListener(this);
      
      add(txtNombre);
      add(chTipoTl);
      add(txtTelefono);
      add(btnGuardar);
      add(log);
   }//construc
   
   public void paint(Graphics g){
      g.drawString("Nombre: ", 20,50);
      g.drawString("Tipo de telefono", 140, 50);
      g.drawString("Telefono", 270, 50);
   }//paint
   
   public void actionPerformed(ActionEvent e){
      if(e.getSource() == btnGuardar){
         log.insert(txtNombre.getText()+"   "+txtTelefono.getText()+"\n", 0);
         JOptionPane.showMessageDialog(null,"Registrado correctamente.");
      }
   }//actionPerformed
   
}//class Frame2