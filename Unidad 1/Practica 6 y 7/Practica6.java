import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;

/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 02/04/2017 como Practica #6
*	para la materia de topicos avanzados de programacion.
*	El siguiente construye un frame que simula una pantalla de ventas de juguetes.
* 	Link del repositorio de la materia: https://bitbucket.org/saidllamas/t-picos-avanzados-de-programaci-n/
*/

public class Practica6 extends Frame implements ActionListener, ItemListener{
   
   TextArea textLog; //Area de logs
   Button btnEfectivo, btnTarjeta, btnFnCp, btnA�adir; //botones a usar
   Choice juguetes, cantidad;
   int costoT, costoR = 0; //Costo del producto y costo por cantidad
   int total = 0; //total a pagar en la venta
   int nCantidad = 1; //Cantidad seleccionada por defecto
   //boolean pintar = false; //control para re-pintar
   String log = ""; //historial de productos
   String producto = ""; //producto individual
   
   public Practica6(){
      setLayout(null);//desactivo el gestor de layout
      
      //construyendo elementos
      btnEfectivo = new Button("Pago en Efectivo");
      btnFnCp = new Button("Finalizar Compra");
      btnTarjeta = new Button("Pago con Tarjeta");
      btnA�adir = new Button("A�adir");
      textLog = new TextArea(5, 20);
      
      juguetes = new Choice();
      juguetes.add(" ");
      juguetes.add("Automovil RC");
      juguetes.add("Drone Phantom");
      juguetes.add("Toy Story Power Blaster Buzz");
      juguetes.add("Iron Man Interactivo");
      juguetes.add("Mu�eca Wellie Wishers Kendall");
      juguetes.add("Mattel Monster High Jellington");
      juguetes.add("Frozen Maletin Juego de Te");
      juguetes.add("Hasbro Monopoly");
      juguetes.add("Hasbro Desafio Pastelazo");
      juguetes.add("Hasbro Rubik�S 3x3");
      
      cantidad = new Choice();
      cantidad.add("1");
      cantidad.add("2");
      cantidad.add("3");
      cantidad.add("4");
      
      //ajustando elementos
      btnFnCp.setBounds(30, 500, 140,28);
      btnEfectivo.setBounds(200, 500, 140,28);
      btnEfectivo.setEnabled(false);
      btnTarjeta.setBounds(370, 500, 140,28);
      btnTarjeta.setEnabled(false);
      btnA�adir.setBounds(410, 130, 100,20);
      juguetes.setBounds(30, 100, 170, 20);
      cantidad.setBounds(240, 100, 170, 20);
      textLog.setBounds(30,160, 480,335);
      textLog.setEditable(false);
      
      //escuchadores
      btnEfectivo.addActionListener(this);
      btnFnCp.addActionListener(this);
      btnTarjeta.addActionListener(this);
      btnA�adir.addActionListener(this);
      juguetes.addItemListener(this);
      cantidad.addItemListener(this);
      
      //a�adiendo al contenedor
      add(btnFnCp);
      add(btnEfectivo);
      add(btnTarjeta);
      add(btnA�adir);
      add(juguetes);
      add(cantidad);
      add(textLog);
   }//constuctor
   
   public void paint(Graphics g){
      g.setFont(new Font("Arial", 0, 25)); //fuente para el titulo
      g.drawString("Ventas Jugueteria",200,60);  //Titulo de app
      g.setFont(new Font("Arial", 0, 14)); //fuente para contenido
      g.drawString("Producto: ", 45, 93);
      g.drawString("Cantidad: ", 260, 93);
      
      g.setFont(new Font("Arial", 0, 27)); //fuente para eL precio
      g.drawString("$ "+costoT*nCantidad, 440 , 115);      //costo temporal del producto (por cantidad)
      
      g.setFont(new Font("Arial", 0, 22)); //fuente para el creador
      g.drawString("Jes�s Said Llamas Manriquez", 140, 575); //Creditos
      
      //dibujo la zona de logs
      //g.setColor(new Color(240, 240, 240)); //gris
      //g.fillRect(30,160, 480,335);  //simulacion de area de texto
      
      //reset color
      g.setColor(Color.BLACK);
      
      //cambios en los choice�s
      /*if(pintar){
         g.setFont(new Font("Arial", 0, 12)); //fuente para logs
         g.drawString(log, 45,175);
      }//pintar*/
   }//paint
   
   public void itemStateChanged(ItemEvent e){

      /*             JUGUETES                */
      if(juguetes.getSelectedItem().equals("Automovil RC")){
         costoT = 560;
      }else if(juguetes.getSelectedItem().equals("Drone Phantom")){
         costoT = 12050;
      }else if(juguetes.getSelectedItem().equals("Toy Story Power Blaster Buzz")){
         costoT = 350;
      }else if(juguetes.getSelectedItem().equals("Iron Man Interactivo")){
         costoT = 650;
      }else if(juguetes.getSelectedItem().equals("Mu�eca Wellie Wishers Kendall")){
         costoT = 220;
      }else if(juguetes.getSelectedItem().equals("Mattel Monster High Jellington")){
         costoT = 430;
      }else if(juguetes.getSelectedItem().equals("Frozen Maletin Juego de Te")){
         costoT = 870;
      }else if(juguetes.getSelectedItem().equals("Hasbro Monopoly")){
         costoT = 590;
      }else if(juguetes.getSelectedItem().equals("Hasbro Desafio Pastelazo")){
         costoT = 775;
      }else if(juguetes.getSelectedItem().equals("Hasbro Rubik�S 3x3")){
         costoT = 260;
      }else{  costoT = 0;  } //no se reconocio el juguete
      producto = ""+juguetes.getSelectedItem(); //recoger el nombre del producto para el log
      
      /*             CANTIDAD               */
      
      if(cantidad.getSelectedItem().equals("1")){
         nCantidad = 1;
      }else if(cantidad.getSelectedItem().equals("2")){
         nCantidad = 2;
      }else if(cantidad.getSelectedItem().equals("3")){
         nCantidad = 3;
      }else if(cantidad.getSelectedItem().equals("4")){
         nCantidad = 4;
      }
      costoR = costoT * nCantidad; //calculo costo total
      
      //pintar = true;
      repaint();
   }//itemStateChanged
   
   public void actionPerformed(ActionEvent e){
      if(producto != ""){
         if(e.getSource() == btnA�adir){ //a�adir producto
            total += costoR;
            //log += nCantidad+"   "+producto+"   $ "+ costoR;
            textLog.insert(nCantidad+"   "+producto+"   $ "+ costoR+ "\n",0);
         }else if(e.getSource() == btnFnCp){ //Finalizar compra
            //log += "                    Total: $ "+total;
            textLog.insert("                    Total: $ "+total+"\n",0);
            btnA�adir.setEnabled(false); //no a�adir mas productos
            btnTarjeta.setEnabled(true); //habilitar forma de pago 1
            btnEfectivo.setEnabled(true); //habilitar forma de pago 2
            btnFnCp.setEnabled(false); //desactivar por lo pronto
            juguetes.setEnabled(false); //bloquear productos
            cantidad.setEnabled(false); //bloquear cantidades
         }
         //log += "\n"; //salto de renglon
         //pintar = true;
      }
      /*       Metodos de pago          */
      if(e.getSource() == btnEfectivo){ //Pagar con efectivo
         JOptionPane.showMessageDialog(this, "�Gracias por su compra!");
         btnA�adir.setEnabled(true);
         btnTarjeta.setEnabled(false);
         btnEfectivo.setEnabled(false);
         btnFnCp.setEnabled(true);
         juguetes.setEnabled(true);
         cantidad.setEnabled(true);
         textLog.replaceRange("",0,200); //la unica manera que encontre para limpiar area
         juguetes.select(0); //seleccion por defecto
         cantidad.select(0); //seleccion por defecto
         costoT = 0;
         total = 0;
         //pintar = true;
         repaint();
      }else if(e.getSource() == btnTarjeta){ //Pagar con tarjeta
         String nombre = JOptionPane.showInputDialog("Nombre del titular: ");
         String telefono = JOptionPane.showInputDialog("Telefono personal: ");
         String ciudad = JOptionPane.showInputDialog("Ciudad actual: ");
         String numTarjeta = JOptionPane.showInputDialog("Numero de tarjeta: ");
         String fechaV = JOptionPane.showInputDialog("Fecha de vencimiento de la tarjeta: ");
         JOptionPane.showMessageDialog(null,"Tienda Sistemas"+"\n"+"Sus datos: "+"\n Titular: "+nombre+"\n Tarjeta: "+numTarjeta+"\n Su total: $ "+total+"\n");
         JOptionPane.showMessageDialog(null,"�Gracias por su compra!");
         textLog.replaceRange("",0,200); //la unica manera que encontre para limpiar area
         juguetes.select(0); //seleccion por defecto
         cantidad.select(0); //seleccion por defecto
         btnA�adir.setEnabled(true);
         btnTarjeta.setEnabled(false);
         btnEfectivo.setEnabled(false);
         btnFnCp.setEnabled(true);
         juguetes.setEnabled(true);
         cantidad.setEnabled(true);
         costoT = 0;
         total = 0;
      }
      System.out.println(log);
      repaint();
   }//actionPerformed
   
   public static void main(String[] args){
      Practica6 app = new Practica6();
      app.setBounds(460, 40, 550, 600); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tama�o
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }//main
}//class
