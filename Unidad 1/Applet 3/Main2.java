import java.awt.*;
import java.applet.*;

public class Main2 extends Applet{
	public void paint(Graphics g){
		//rombo
		Polygon rombo = new Polygon();
		rombo.addPoint(120, 210);
		rombo.addPoint(20, 110);
		rombo.addPoint(120, 10);
		rombo.addPoint(220, 110);
		g.fillPolygon(rombo);
		//trapecio
		Polygon trapecio = new Polygon();
		trapecio.addPoint(260, 170);
		trapecio.addPoint(300, 70);
		trapecio.addPoint(420, 70);
		trapecio.addPoint(460, 170);
		g.fillPolygon(trapecio);

		//estrella 4icos
		g.fillRect(100, 350, 80, 80);//dibujo base
		Polygon est4 = new Polygon();
		est4.addPoint(100, 350);
		est4.addPoint(140, 300);
		est4.addPoint(180, 350);//end pico arriba
		g.fillPolygon(est4);
		est4.reset();
		est4.addPoint(100, 430);
		est4.addPoint(140, 480);
		est4.addPoint(180, 430);//end pico abajo
		g.fillPolygon(est4);
		est4.reset();
		est4.addPoint(180, 350);
		est4.addPoint(230, 390);
		est4.addPoint(180, 430);//end pico der
		g.fillPolygon(est4);
		est4.reset();
		est4.addPoint(100, 350);
		est4.addPoint(50, 390);
		est4.addPoint(100, 430);//end pico izq
		g.fillPolygon(est4);

		//estrella 5icos
		Polygon est5 = new Polygon();
		est5.addPoint(350, 350);
		est5.addPoint(390, 300);
		est5.addPoint(430, 350);//end pico arriba
		est5.addPoint(430, 350);
		est5.addPoint(480, 350);
		est5.addPoint(430, 420);//end pico der
		est5.addPoint(430, 420);
		est5.addPoint(480, 500);
		est5.addPoint(390, 450);//end pico der2
		est5.addPoint(390, 450);
		est5.addPoint(310, 500);
		est5.addPoint(350, 420);//end pico izq2
		est5.addPoint(350, 420);
		est5.addPoint(300, 350);
		est5.addPoint(350, 350);//end pico izq
		g.drawPolygon(est5);

		//fig lin cuadradas
		int xC[] = {40,  40,  50,  50,  60,  60,  70,  70,  80,  80, 90, 90, 100, 100, 110, 110};
		int yC[] = {580, 600, 600, 580, 580, 600, 600, 580, 580, 600, 600, 580, 580, 600, 600 , 580};
		g.drawPolyline(xC, yC, 16);

		//fig lin triangulo
		int xT[] = {210, 220, 230, 240, 250, 260, 270, 280,290};
		int yT[] = {600, 580, 600, 580, 600, 580, 600, 580,600};
		g.drawPolyline(xT, yT, 9);
	}
}