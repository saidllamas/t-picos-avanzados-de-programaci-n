import java.awt.*;
import java.applet.*;

public class Main extends Applet{
	public void paint(Graphics g){

		//arriba
		g.drawArc(270, 70,70,50, 0, 180); //Nube
		g.drawArc(320, 70,70,50, 0, 180); //Nube
		g.drawArc(370, 70,70,50, 0, 180); //Nube
		g.drawArc(420, 70,70,50, 0, 180); //Nube
		//abajo
		g.drawArc(270, 110,70,50, 0, -180); //Nube
		g.drawArc(320, 110,70,50, 0, -180); //Nube
		g.drawArc(370, 110,70,50, 0, -180); //Nube
		g.drawArc(420, 110,70,50, 0, -180); //Nube
		//lat izq
		g.drawArc(446,78, 50,70, -270, -180); //Nube
		g.drawArc(263,78, 50,70, -270, 180); //Nube


		g.fillArc(50, 50, 170, 170, 35, 290); //pacma contorno
		g.setColor(Color.WHITE);
		g.fillOval(80, 80, 40, 40); //ojo


		//corazon
		g.setColor(Color.BLACK);
		g.drawArc(50, 270, 90, 70, 0, 180);
		g.drawArc(140, 270, 90, 70, 0, 180);
		g.drawLine(51, 304, 140, 420); //izq
		g.drawLine(230,304, 140, 420); //der 

//+40
		g.drawOval(270, 210, 175, 200); //contorno cabeza 
		g.drawOval(310, 270, 20, 20); //ojo izquierdo
		g.fillOval(312, 278, 16, 14); //pupila izquierdo
		g.drawOval(385, 270, 20, 20); //ojo derecho
		g.fillOval(387, 278, 16, 14); //pupila derecha
		g.drawOval(340, 310, 30, 30);//boca
		//g.drawOval(173, 320, 15, 32);//gota
		g.drawArc(317, 305,80,80, 0, -180); //boca
		g.drawArc(312, 261,16,10, 0, 180); //ojo izq
		g.drawArc(388, 261,16,10, 0, 180); //ojo izq
	}
}