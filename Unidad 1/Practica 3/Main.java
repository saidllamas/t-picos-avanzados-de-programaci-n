import java.applet.*;
import java.awt.*;

public class Main extends Applet{

   public static final Color YELLOW = new Color(255, 235, 59);
   public static final Color BLACK = new Color(33, 33, 33);
   public static final Color GREEN = new Color(76, 175, 80);
   public static final Color BLUE = new Color(3, 169, 244);
   public static final Color ORANGE = new Color(255,87,34);
   public static final Color RED = new Color(244,67,54);
   
   
   public void init(){
      Dimension d = Toolkit.getDefaultToolkit().getScreenSize(); //obtengo medidas de pantalla
      resize(d.width, d.height);//ejecuto a pantalla completa
      setBackground(new Color(255, 249, 196)); 
   }//end init
   
   public void paint(Graphics g){
      //Ajustes de pantalla
      int inicioX = 80, inicioY = 120;
      //Personalizacion
      g.setFont(new Font("Calibri", Font.BOLD + Font.ITALIC, 30));
      g.setColor(BLACK); //Negro especial
		g.drawString("J. Said Llamas Manriquez I.S.C M-11", 500, 45);
      //abeja
         //cuerpo contorno
         g.setColor(YELLOW);
         g.fillOval(inicioX, inicioY, 40, 40);//panza
            //detalles panza
            g.setColor(Color.BLACK);
            g.drawArc(inicioX+3, inicioY+3, 17,33, -270,180);
            g.drawArc(inicioX+10, inicioY+1, 17,37, -270,180);
            g.drawArc(inicioX+17, inicioY+1, 17,39, -270,180);
            g.drawArc(inicioX+24, inicioY+3, 17,33, -270,180);
         g.setColor(YELLOW);
         g.fillOval(inicioX+30, inicioY-20, 40, 40); //Cabeza
         //detalles cabeza
            g.setColor(Color.BLACK);
            g.fillOval(inicioX+53, inicioY-10, 10,8);//Ojo
            g.drawArc(inicioX+52, inicioY+3, 14,8, 0,-180);//boca
            g.drawArc(inicioX+48,inicioY-37, 7,15, -270, -180);//antena iz
            g.drawArc(inicioX+62,inicioY-30, 7,15, -270, 180);//antena
         //alas
            //ala arriba
            g.setColor(BLUE);
            int [] aaX = {inicioX-2, inicioX+10, inicioX+20};
            int [] aaY = {inicioY-15, inicioY+2, inicioY-24};
            g.fillPolygon (aaX, aaY, 3);
            //ala izq
            int [] aiX = {inicioX+6, inicioX-10, inicioX-25};
            int [] aiY = {inicioY+5, inicioY-17, inicioY-12};
            g.fillPolygon (aiX, aiY, 3);
      //Gusano
         //Cabeza
         g.setColor(GREEN);
         g.fillOval(inicioX+100,inicioY-20, 40,40);      
         g.setColor(BLACK);
         g.fillOval(inicioX+106, inicioY-10, 8, 8);
         g.drawArc(inicioX+103, inicioY, 14,8, 0,-180);//boca
         g.drawArc(inicioX+111,inicioY-33, 7,15, -270, 180);//antena
         //Cuerpo
         g.setColor(GREEN);
         g.fillOval(inicioX+128,inicioY+5, 40,40);
         g.fillOval(inicioX+166,inicioY+10, 40,40);
         g.fillOval(inicioX+204,inicioY+5, 40,40);
         g.fillOval(inicioX+242,inicioY+8, 40,40);
         g.fillOval(inicioX+280,inicioY+4, 40,40);
         g.setColor(BLACK);
         g.drawArc(inicioX+140,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+160,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+170,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+190,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+210,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+230,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+250,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+270,inicioY+36, 7,15, -270, 180);//pata
         g.drawArc(inicioX+290,inicioY+36, 7,15, -270, -180);//pata iz
         g.drawArc(inicioX+310,inicioY+36, 7,15, -270, 180);//pata
      //Globos
         g.setColor(BLUE);
         //globos
         g.fillOval(inicioX+360, inicioY-50, 60, 60);
         g.fillOval(inicioX+370, inicioY-38, 60, 60);
         //globos
         g.setColor(GREEN);
         g.fillOval(inicioX+480, inicioY-50, 60, 60);
         g.fillOval(inicioX+470, inicioY-38, 60, 60);
         //globos
         g.setColor(YELLOW);
         g.fillOval(inicioX+570, inicioY-2, 60, 60);
         g.fillOval(inicioX+560, inicioY+10, 60, 60);
         //listones
         int [] lsX = {inicioX+580, inicioX+576, inicioX+563, inicioX+550, inicioX+517,inicioX+512,inicioX+504, inicioX+500};
         int [] lsY = {inicioY+68, inicioY+72, inicioY+76, inicioY+87, inicioY+102, inicioY+107, inicioY+117, inicioY+129};
         g.setColor(BLACK);
         g.drawPolyline(lsX, lsY, 8);
         int [] ls2X={inicioX+490, inicioX+485,inicioX+480, inicioX+475, inicioX+470};
         int [] ls2Y={inicioY+20, inicioY+27, inicioY+40, inicioY+60, inicioY+129};
         g.drawPolyline(ls2X, ls2Y, 5);
         int [] ls3X={inicioX+400, inicioX+410,inicioX+420, inicioX+430, inicioX+450, inicioX+465};
         int [] ls3Y={inicioY+20, inicioY+27, inicioY+40, inicioY+60, inicioY+100, inicioY+129};
         g.drawPolyline(ls3X, ls3Y, 6);
      //Papalote
         //fondo
         Polygon rombo = new Polygon();
   		rombo.addPoint(inicioX+750, inicioY+50);
   		rombo.addPoint(inicioX+680, inicioY+10);
   		rombo.addPoint(inicioX+722, inicioY-50);
   		rombo.addPoint(inicioX+780, inicioY-10);
         g.setColor(GREEN);
   		g.fillPolygon(rombo);
         //superpuesto
         rombo.reset();
         rombo.addPoint(inicioX+745, inicioY+40); 
   		rombo.addPoint(inicioX+710, inicioY+20);
   		rombo.addPoint(inicioX+723, inicioY+2);
   		rombo.addPoint(inicioX+755, inicioY+20);
         g.setColor(YELLOW);
   		g.fillPolygon(rombo);
         //Listones
         int [] lsPX = {inicioX+745, inicioX+750, inicioX+760, inicioX+755, inicioX+750, inicioX+760};
         int [] lsPY = {inicioY+45, inicioY+60, inicioY+75, inicioY+90, inicioY+115, inicioY+130};
         g.setColor(BLACK);
         g.drawPolyline(lsPX, lsPY, 6);
         Polygon mo�o = new Polygon();
         mo�o.addPoint(inicioX+745, inicioY+80);
         mo�o.addPoint(inicioX+745, inicioY+100);
         mo�o.addPoint(inicioX+780, inicioY+80);
         mo�o.addPoint(inicioX+780, inicioY+100);
         g.fillPolygon(mo�o);
      //Flor
         //maceta
         g.setColor(GREEN);
         g.fillRect(inicioX, inicioY+190, 45, 50);
         g.setColor(BLUE);
         g.fillRoundRect(inicioX-5, inicioY+180, 55, 14, 4,4);
         //Flor
            //contorno
            g.setColor(ORANGE);
            g.fillOval(inicioX+5, inicioY+110, 30, 30);//arriba
            g.fillOval(inicioX+20, inicioY+123, 30, 30);//der
            g.fillOval(inicioX-8, inicioY+123, 30, 30);//izq
            g.fillOval(inicioX+5, inicioY+135, 30, 30);//abajo
            //centro
            g.setColor(YELLOW);
            g.fillOval(inicioX+3, inicioY+120, 35, 35);
            //rama
            int [] lsRX = {inicioX+36, inicioX+40,inicioX+42,inicioX+38,inicioX+32};
            int [] lsRY = {inicioY+150,inicioY+163, inicioY+165, inicioY+171, inicioY+183};
            g.setColor(BLACK);
            g.drawPolyline(lsRX, lsRY, 5);
      //Reilete
         //palo
         inicioX = 0; //AJUSTE EN X
         g.fillRect(inicioX+240, inicioY+180, 3, 70);   
         //cuadrados
         g.setColor(BLUE);
         g.fillRect(inicioX+240, inicioY+160, 20, 20);
         g.fillRect(inicioX+221, inicioY+140, 20, 20);
         g.setColor(YELLOW);
         g.fillRect(inicioX+240, inicioY+140, 20, 20);
         g.fillRect(inicioX+221, inicioY+160, 20, 20);
         //Triangulos
         Polygon triangulo = new Polygon();
         triangulo.addPoint(inicioX+243,inicioY+200);
         triangulo.addPoint(inicioX+243,inicioY+180);
         triangulo.addPoint(inicioX+260,inicioY+180);
         g.setColor(BLUE);
         g.fillPolygon(triangulo);
         triangulo.reset();
         g.setColor(YELLOW);
         triangulo.addPoint(inicioX+260,inicioY+140);
         triangulo.addPoint(inicioX+280,inicioY+160);
         triangulo.addPoint(inicioX+260,inicioY+160);
         g.fillPolygon(triangulo);
         triangulo.reset();
         g.setColor(BLUE);
         triangulo.addPoint(inicioX+220, inicioY+140);
         triangulo.addPoint(inicioX+240, inicioY+120);
         triangulo.addPoint(inicioX+240, inicioY+140);
         g.fillPolygon(triangulo);
         triangulo.reset();
         g.setColor(YELLOW);
         triangulo.addPoint(inicioX+200, inicioY+180);
         triangulo.addPoint(inicioX+221, inicioY+160);
         triangulo.addPoint(inicioX+221, inicioY+180);
         g.fillPolygon(triangulo);
         
         inicioX = 80;
      //Abeja
         //Alas
         g.setColor(ORANGE);
         rombo.reset();
   		rombo.addPoint(inicioX+283, inicioY+242);
   		rombo.addPoint(inicioX+263, inicioY+192);
   		rombo.addPoint(inicioX+283, inicioY+142);
   		rombo.addPoint(inicioX+303, inicioY+192);
   		g.fillPolygon(rombo);
         //derecha
         rombo.reset();
   		rombo.addPoint(inicioX+355, inicioY+242);
   		rombo.addPoint(inicioX+335, inicioY+192);
   		rombo.addPoint(inicioX+355, inicioY+142);
   		rombo.addPoint(inicioX+375, inicioY+192);
   		g.fillPolygon(rombo);
         //cuerpo
         g.setColor(GREEN);
         g.fillOval(inicioX+300, inicioY+163, 40, 40);
         g.fillOval(inicioX+300, inicioY+200, 40, 40);
         g.setColor(BLACK);
         g.fillOval(inicioX+306, inicioY+170, 7,7);
         g.fillOval(inicioX+316, inicioY+170, 7,7);
         g.drawArc(inicioX+310, inicioY+180 , 10,10, 0, -180); //sonrisa
      //sol-nubes
         g.setColor(YELLOW);
         g.fillOval(inicioX+500, inicioY+160, 50,50);   //Sol
         //Nubes
         g.setColor(BLUE);
         g.fillOval(inicioX+485, inicioY+192, 40, 40);
         g.fillOval(inicioX+520, inicioY+192, 40, 40);
         g.fillOval(inicioX+540, inicioY+210, 40, 40);
         g.fillOval(inicioX+520, inicioY+228, 40, 40);
         g.fillOval(inicioX+493, inicioY+220, 48, 48);
      //arbol
         //tallo
         g.setColor(BLACK);
         g.fillRect(inicioX+45, inicioY+440, 50, 80);
         //hojas
         g.setColor(GREEN);
         g.fillOval(inicioX, inicioY+300, 80, 80);   
         g.fillOval(inicioX+50, inicioY+300, 80, 80);
         g.fillOval(inicioX+85, inicioY+340, 80, 80);
         g.fillOval(inicioX-40, inicioY+340, 80, 80);
         g.fillOval(inicioX+5, inicioY+340, 90, 90);//Relleno
         g.fillOval(inicioX+50, inicioY+380, 80, 80);
         g.fillOval(inicioX, inicioY+380, 80, 80);
         //Manzana
         g.setColor(ORANGE);
         g.fillOval(inicioX+20,inicioY+320, 40,40);
         g.fillOval(inicioX,inicioY+379, 40,40);
         g.fillOval(inicioX+90,inicioY+349, 40,40);
      //Barco
         //caja
         Polygon trapecio = new Polygon();
   		trapecio.addPoint(300, inicioY+400);
         trapecio.addPoint(340, inicioY+450);
   		trapecio.addPoint(420, inicioY+450);
         trapecio.addPoint(460, inicioY+400);
         g.setColor(YELLOW);
   		g.fillPolygon(trapecio);
         //Circulo
         g.setColor(ORANGE);
         g.fillOval(inicioX+310, inicioY+410, 30, 30);
         g.fillOval(inicioX+260, inicioY+410, 30, 30);
         g.setColor(BLACK);
         //Bandera
            //Asta
            g.fillRect(inicioX+300, inicioY+330, 3, 70);
            //bandera
            g.setColor(GREEN);
            triangulo.reset();
            triangulo.addPoint(inicioX+302,inicioY+330);
            triangulo.addPoint(inicioX+350,inicioY+350);
            triangulo.addPoint(inicioX+302,inicioY+360);
            g.fillPolygon(triangulo);
         //Escarabajos
            //peque�o
            g.setColor(RED);
            g.fillOval(inicioX+460, inicioY+350, 60, 60);
            g.setColor(BLACK);
            g.fillOval(inicioX+516, inicioY+374,14,14);//CABEZA
            //division
            Polygon division = new Polygon();
            division.addPoint(inicioX+460, inicioY+380);
            division.addPoint(inicioX+520, inicioY+380);
            g.drawPolygon(division);
            division.reset();
            //Puntos
            g.fillOval(inicioX+480, inicioY+390, 4,4);
            g.fillOval(inicioX+483, inicioY+358, 4,4);
            g.fillOval(inicioX+498, inicioY+367, 4,4);
            g.fillOval(inicioX+473, inicioY+370, 4,4);
            g.fillOval(inicioX+505, inicioY+394, 4,4);
            //Grande
            g.fillOval(inicioX+650, inicioY+344,40,40);//CABEZA
            g.setColor(RED);
            g.fillOval(inicioX+590, inicioY+300, 90, 90);
            //divisioN
            g.setColor(BLACK);
            division.addPoint(inicioX+680, inicioY+360);
            division.addPoint(inicioX+590, inicioY+330);
            g.drawPolygon(division);
            //Puntos
            g.fillOval(inicioX+650, inicioY+360, 4,4);
            g.fillOval(inicioX+630, inicioY+310, 4,4);
            g.fillOval(inicioX+610, inicioY+323, 4,4);
            g.fillOval(inicioX+623, inicioY+318, 4,4);
            g.fillOval(inicioX+633, inicioY+368, 4,4);
            g.fillOval(inicioX+658, inicioY+327, 4,4);
            g.fillOval(inicioX+628, inicioY+347, 4,4);
            g.fillOval(inicioX+646, inicioY+317, 4,4);
            g.fillOval(inicioX+616, inicioY+347, 4,4);
            g.fillOval(inicioX+623, inicioY+370, 4,4);
            g.fillOval(inicioX+655, inicioY+374, 4,4);

         //Camion
            g.setColor(ORANGE);
            g.fillRect(inicioX+460, inicioY+440, 80, 50);
            g.setColor(GREEN);
            g.fillRect(inicioX+460, inicioY+480, 80, 20);
            g.setColor(ORANGE);
            g.fillRect(inicioX+440, inicioY+460, 20, 40);
            g.fillRect(inicioX+420, inicioY+480, 20, 20);
            g.setColor(GREEN);
            g.fillRect(inicioX+420, inicioY+460, 20, 20);
            //Llantas
            g.setColor(BLUE);
            g.fillOval(inicioX+430, inicioY+490, 22, 22);
            g.fillOval(inicioX+460, inicioY+490, 22, 22);
            g.fillOval(inicioX+490, inicioY+490, 22, 22);
            g.fillOval(inicioX+520, inicioY+490, 22, 22);
         //Pollos
            //grande
            g.setColor(YELLOW);
            g.fillOval(inicioX+620, inicioY+460, 60, 60); 
            g.fillOval(inicioX+627, inicioY+435, 40, 40);
            //Pico
            g.setColor(ORANGE);
            Polygon pico = new Polygon();
            pico.addPoint(inicioX+630, inicioY+450);
            pico.addPoint(inicioX+640, inicioY+460);
            pico.addPoint(inicioX+610, inicioY+450);
            g.fillPolygon(pico);
            g.setColor(BLACK);
            g.fillOval(inicioX+643, inicioY+445, 8, 8);
            
            //Peque�o
            g.setColor(YELLOW);
            g.fillOval(inicioX+720, inicioY+460, 40, 40); 
            g.fillOval(inicioX+727, inicioY+446, 20, 20);
            //pico
            pico.reset();
            g.setColor(ORANGE);
            pico.addPoint(inicioX+730, inicioY+450);
            pico.addPoint(inicioX+730, inicioY+455);
            pico.addPoint(inicioX+720, inicioY+450);
            g.fillPolygon(pico);
            g.setColor(BLACK);
            g.fillOval(inicioX+730, inicioY+450, 4, 4);
         
   }//end paint
   
}//class
